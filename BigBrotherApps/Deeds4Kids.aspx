﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Deeds4Kids.aspx.cs" Inherits="BigBrother.Rewardboard" %>
<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
            <title>Deeds4Kids App Case Study</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Breadcrumb -->
    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="Default.aspx">Home</a></li>
                <li><a href="Portfolio.aspx">Portfolio</a></li>
                <li class="active">Deeds4Kids</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Product Content -->
    <div class="container ws-page-container">
        <div class="row">

            <!-- Product Image Carousel -->
            <div class="col-sm-5">
                <%--<div id="ws-products-carousel" class="owl-carousel">
                    <div class="item">
                        <img src="assets/img/works/Rewardboard/1.png" class="img-responsive" />
                    </div>
                    <div class="item">
                        <img src="assets/img/works/Rewardboard/2.png" class="img-responsive" />
                    </div>
                    <div class="item">
                        <img src="assets/img/works/Rewardboard/3.png" class="img-responsive" />
                    </div>
                </div>--%>
                 <iframe src="https://marvelapp.com/6j15i0a?emb=1" width="100%" height="801" allowTransparency="true" frameborder="0"></iframe>
            </div>

            <!-- Product Information -->
            <div class="col-sm-7">
                <div class="ws-product-content">
                    <header>
                        <!-- Item Category -->

                        <!-- Title -->
                        <h3 class="ws-item-title">Deeds4Kids</h3>

                        <div class="ws-separator"></div>

                        <!-- Price -->


                        <!-- Quantity -->
                        <a target="_blank" href="https://itunes.apple.com/us/app/rewardboard/id1084960289?mt=8"><i class="fa fa-apple text-center fontsize24"></i></a>&nbsp;
                         <a target="_blank" href="https://play.google.com/store/apps/details?id=com.impero.rewardboard&hl=en"><i class="fa fa-android text-center fontsize24"></i></a>
                    </header>

                    <div class="ws-product-details">
                        <h3>Project Baseline</h3>
                        <p>To create a practical application for parents to track on the activities performed by their children in order to encourage positive behavior.</p>
                        <br />
                        <br />
                        <h3>The Problems and Solutions</h3>
                        <p>
                            Finding enough time in the day and balancing parenting and career is a very common problem nowadays whereby if the children are not given full attention to their behavior and habits then it can lead to a poor-quality life ahead.
                            <br />
                            <br />
                            Few are common behavioral problems which needs to be looked upon?
                            <br />
                            <br />
                            -	Staying awake until late which results to having lack of sleep.
                            <br />
                            -	Refusal for helping in chores such as clearing up the dishes.
                            <br />
                            -	Ignoring priority tasks such as Homework and reading.
                            <br />
                            -	Using & practicing foul languages heard from people/Internet.
                            <br />
                            -	Health care, focusing on bathing and brushing teeth on time.
                           


                        </p>

                    </div>
                    <!-- Button -->
                    <a class="btn ws-big-btn" href="Contact.aspx">Let's Talk!</a>
                    <%--<a class="btn ws-big-btn" href="#ws-register-modal" data-toggle="modal">Let's Talk!</a><Contactform:UserInfoBoxControl runat="server" ID="MyUserInfoBoxControl" />--%>
                </div>
            </div>
           
            <div class="col-sm-12"> <br />
            <br />
            <br />
                Ever thought about gadget/technology addiction? Assuming yes, as this is the most dangerous act which leads to a disturbed life schedule and if its compared to the above problems then almost all of them are linked because of gadget addiction.
                <br />
                <br />
                What can be done to improve the growth of your child? Read the solution below.<br />
                <br />
                After conducting several research sessions, it was decided to build an application which will work similar to reward charts whereby it would encourage and motivate the kids to behave well by rewarding them. This will result to having the kids practice or perform the same activities which are usually ignored and denied.
                <br />

                <br />
            </div>
        </div>
    </div>
    <div class="ws-products-description-content text-center">

        <!-- Item -->

        <!-- Item -->
        <div class="ws-product-description">
            <h3>App Features</h3>
            <ul class="item">
                <li class="list-group-item">Facebook login as an easier way of completing the registration process.
                </li>
                <li class="list-group-item">A trending UI design which impresses and encourages the users to keep on using the                  application.
                </li>
                <li class="list-group-item">Add children with simple registration screen. 
                </li>
                <li class="list-group-item">Add tasks for the children. In addition, select days of the week each task should be performed and up to 5 tasks are available for free.
                </li>
                <li class="list-group-item">Add points once the tasks have been performed. In addition, Points can be reduced if they were added by mistake.
                </li>
                <li class="list-group-item">Payout the reward for the tasks performed to encourage children to keep up the good behavior and up to 8 payouts are available for free. In addition, view all the rewards given and the payout date.
                </li>
                <li class="list-group-item">View the points for each week and compare if the results obtained now are better than before or to see how far you have come.
                </li>
                <li class="list-group-item">A multilingual application offering English and Danish as language options.
                </li>
                <li class="list-group-item">Premium users to benefit unlimited tasks, unlimited payouts/rewards and Ad free application at a cost of around $1.30 only.


                </li>

            </ul>
            <br />
            <br />
            <h3>Project Highlights</h3>
            <p style="color:#000 !important;">

                The application is built on both Android and iOS so that maximum number of parents should be able to use this system in order to improve their child’s upbringing. The application has more than 6000 parents, 9000+ children added, 5000+ tasks added and more than 1700 rewards created. 
            </p>
        </div>
    </div>

</asp:Content>


