﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="CoolOff.aspx.cs" Inherits="BigBrother.CoolOff" %>
<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
            <title>Cooloff App Case Study</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Breadcrumb -->
    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="Default.aspx">Home</a></li>
                <li><a href="Portfolio.aspx">Portfolio</a></li>
                <li class="active">CoolOff</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Product Content -->
    <div class="container ws-page-container">
        <div class="row">

            <!-- Product Image Carousel -->
            <div class="col-sm-5">
               <%-- <div id="ws-products-carousel" class="owl-carousel">
                    <div class="item">
                        <img src="assets/img/works/CoolOff/1.png" class="img-responsive" />
                    </div>
                    <div class="item">
                        <img src="assets/img/works/CoolOff/2.png" class="img-responsive" />
                    </div>
                    <div class="item">
                        <img src="assets/img/works/CoolOff/3.png" class="img-responsive" />
                    </div>
                </div>--%>

                <iframe src="https://marvelapp.com/2673d28?emb=1" width="100%" height="801" allowTransparency="true" frameborder="0"></iframe>
            </div>

            <!-- Product Information -->
            <div class="col-sm-7">
                <div class="ws-product-content">
                    <header>
                        <!-- Item Category -->

                        <!-- Title -->
                        <h3 class="ws-item-title">CoolOff</h3>

                        <div class="ws-separator"></div>

                        <!-- Price -->


                        <!-- Quantity -->
                        <a target="_blank" href="https://itunes.apple.com/us/app/cool-off-app/id1119480248?ls=1&mt=8"><i class="fa fa-apple text-center fontsize24"></i></a>&nbsp;
                         <a target="_blank" href="https://play.google.com/store/apps/details?id=com.cooloff.app"><i class="fa fa-android text-center fontsize24"></i></a>
                    </header>

                    <div class="ws-product-details">
                        <h3>Project Baseline</h3>
                        <p>To create an application to reduce the usage of Ac’s and increase the use of greener method such as opening windows and using whole fans when the desired temperature is reached.</p>
                        <br />
                        <h3>The Problems and Solutions</h3>
                        <p>
                            There are two main problems when it comes to using air conditioners at our home, office and even in the public nowadays. 
                           <br />
                            <br />
                            <b>1. The first main problem relates to the life.</b><br />
                            Ac’s are used almost everywhere to fight the heat and humid temperature in order to feel comfortable but financially huge amounts are paid for electricity bills and in terms of health, many people do not realize that there are severe chances of being attacked with illnesses if they completely rely on it 24 hours a day, 7 days a week such as effects on the blood, hair loss, taste changes, tiredness, etc.
                           <br />
                            <br />
                            <b>The second main problem is relating to the atmosphere.</b><br />
                            Did you know that the Ac’s uses 10-20 times more power and electricity then whole fans? Ac’s are now one of the major sources of air pollution because of high number of electricity/power consumption and also emitting dangerous gases into the air.
                            <br />
                            <br />
                        </p>
                    </div>
                    <!-- Button -->
                    <a class="btn ws-big-btn" href="Contact.aspx">Let's Talk!</a>
                     <%--<a class="btn ws-big-btn" href="#ws-register-modal" data-toggle="modal">Let's Talk!</a><Contactform:UserInfoBoxControl runat="server" ID="MyUserInfoBoxControl" />--%>
                </div>
            </div>
            <br />
            <div class="col-sm-12"> <br /> <br />
                It’s known that it’s next to impossible to stop using the Ac’s due to the blazing atmosphere unless it gets cooler whereas people usually do get annoyed to often check out the weather conditions outside.
            <br />
                <br />
                If an individual can save three hours a day by not using the AC then how much would he/she save at the end of month in terms of electricity bills and how much pollution has he/she stopped by not using the AC? Just imagine! If millions of people stopped using the AC when not needed.<br />
                <br />
                These are the reasons for why cool-off app is brought up in the market.<br />
                <br />
                If a user is comfortable with 27 degrees Celsius then cool off will detect the temperature outside and notify the user once it gets cooler outside so that AC can be turned off to use greener methods.
            <br />
                <br />
                All that needs to be done for setting up is, add the location, customize the preferences, start monitoring and get notified to turn off your AC and to start using greener methods to cool your area.
            <br />
            </div>
        </div>
    </div>
    <div class="ws-products-description-content text-center">

        <!-- Item -->

        <!-- Item -->
        <div class="ws-product-description">
            <h3>App Features:</h3>
            <ul class="item">
                <li class="list-group-item">Notification will only arrive when the temperature outside gets cooler for the rest of the day.
                </li>
                <li class="list-group-item">A single account can be used for multiple devices.
                </li>
                <li class="list-group-item">View detailed status such as viewing the current temperature outside, approximate time you can switch of your AC for the rest of the day and many more.
                </li>
                <li class="list-group-item">Premium users to benefit an AD free application and also the upcoming feature which can automatically switch off the AC when the desired temperature is met.



                </li>

            </ul>
        </div>
    </div>
</asp:Content>

