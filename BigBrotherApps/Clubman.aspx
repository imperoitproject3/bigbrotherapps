﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Clubman.aspx.cs" Inherits="BigBrother.Clubman" %>

<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">        <title>Clubman App Case Study</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Breadcrumb -->
    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="Default.aspx">Home</a></li>
                <li><a href="Portfolio.aspx">Portfolio</a></li>
                <li class="active">Clubman</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Product Content -->
    <div class="container ws-page-container">
        <div class="row">

            <!-- Product Image Carousel -->
            <div class="col-sm-5">
                <iframe src="https://marvelapp.com/4289gca?emb=1" width="100%" height="801" allowTransparency="true" frameborder="0"></iframe>
            </div>

            <!-- Product Information -->
            <div class="col-sm-7">
                <div class="ws-product-content">
                    <header>
                        <!-- Item Category -->

                        <!-- Title -->
                        <h3 class="ws-item-title">The Hurler</h3>

                        <div class="ws-separator"></div>

                        <!-- Price -->


                        <!-- Quantity -->
                        <a  target="_blank" href="https://itunes.apple.com/us/app/clubman/id1233698692?mt=8"><i class="fa fa-apple text-center fontsize24"></i></a>&nbsp;
                         <a target="_blank" href="https://play.google.com/store/apps/details?id=com.clubmanlive&hl=en"><i class="fa fa-android text-center fontsize24"></i></a>
                    </header>

                    <div class="ws-product-details">
                        <h3>Project Baseline</h3>
                        <p>To create an application for providing/reporting live updates for the GAA sports matches via twitter.</p>
                        <br />
                        <br />
                        <h3>The Problems and Solutions</h3>
                        <p>
                            GAA sports are played in Ireland under the auspices of the Gaelic Athletic Association. There are the two main sports played, Football and the Hurling. There are other games also organized by GAA such as handball, rounders etc. Did you know: The Hurling game has been played for 3000+ years!
                            <br />
                            <br />
                            The application has integrated another feature which is, the tweet post is also posted in user’s city’s group so that the post reaches to a large number of people.
                            <br /> <br />
                            <i>All the players/supporters/Manager/Coach/Organizer of the GAA sports can make use of such applications to easily keep track and updates follower with latest score of the matches via Twitter Application. </i>

                        </p>

                    </div>
                    <!-- Button -->
                    <a class="btn ws-big-btn" href="Contact.aspx">Let's Talk!</a>
                    <%--<a class="btn ws-big-btn" href="#ws-register-modal" data-toggle="modal">Let's Talk!</a><Contactform:UserInfoBoxControl runat="server" ID="MyUserInfoBoxControl" />--%>
                </div>
            </div>
        </div>
    </div>
    <div class="ws-products-description-content text-center">

        <!-- Item -->

        <!-- Item -->
        <div class="ws-product-description">
            <h3>App Features</h3>
            <ul class="item">
                <li class="list-group-item">Email and password login. </li>
                <li class="list-group-item">Tweet with the unique templates provided by the application.
                </li>
                <li class="list-group-item">Schedule a game by: Selecting the game’s grade level, Adding the team names, adding match’s time length, adding players and creating the team line up, entering the competition’s name, entering hashtags (#/@) if needed and START!
                </li>
                <li class="list-group-item">Record each and every goal scored and award points for who scored it, how he scored it, and the application will note down the time of the goal. In addition, there is an ability to remove the goals and points in case they were added by mistake or any other reason.
                </li>
                <li class="list-group-item">Substitute players if an injury has occurred, etc.
                </li>
                <li class="list-group-item">If it was a draw match then either end the game or continue to extra time.
                </li>
                <li class="list-group-item">At the end of the match, the whole score board will be displayed in a single screen and users can tweet the screen directly with the unique tweet templates.


                </li>

            </ul>
        </div>
    </div>
</asp:Content>


