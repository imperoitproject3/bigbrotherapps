﻿using ImperoIT;
using BigBrother.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BigBrother.DAL;

namespace BigBrother
{
    public partial class Blog : System.Web.UI.Page
    {
        public static string ToTitle(string value)
        {
            return value.Replace(" ", "-").ToLower();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindBlogs();
            }
        }

        private void BindBlogs()
        {
            BigBrotherEntities _ctx = new BigBrotherEntities();
            List<BlogMasterModel> blogs = (from b in _ctx.BlogMaster
                                           select new BlogMasterModel
                                           {
                                               BlogId = b.BlogId,
                                               Title = b.Title,
                                               Tag = b.Tag,
                                               Author = b.Author,
                                               ImageUrl = b.ImageUrl,
                                               PublishDate = b.PublishDate
                                           }).ToList();

            if (blogs != null && blogs.Any())
            {
                //foreach (var item in blogs)
                //{
                //    item.Title = ToTitle(item.Title);
                //}
                RV.DataSource = blogs;
            }
            else
                RV.DataSource = null;
            RV.DataBind();
        }
    }
}