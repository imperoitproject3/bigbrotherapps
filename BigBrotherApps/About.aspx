﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="BigBrother.About" %>

<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>About Big Brother Apps</title>
    <style>
        .ws-item-title {
            font-size: 17px !important;
            margin-top: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="javascript:;">About</a></li>
                <li class="active">Big Brother Apps</li>
            </ol>
        </div>
    </div>

    <br />
    <br />
    <!-- Page Content -->
    <div class="container our-services">
        <div class="row">
            <section class="our-services ws-subscribe-content home2">
                <div class="container">
                    <p class="text-center">
                        Big Brother Apps is an Australian software development company with core expertise in mobile application development (across both iOS and Android) and website development. We have an experienced team of developers and other IT professionals to help you to achieve your goals.
                    <br />
                        <br />
                        From a buisness perspective our services extend beyond just software development. We are capable and experienced in a number of specialisations to ensure success. This includes providing assistance in marketing strategy and implementation, company structuring, government grant applications and capital raising.
                    <br />
                        <br />
                        At Big Brother Apps customer service is our key focus. Our team are fully committed to your cause and we prove it through our extended app support system. Our vision is to assist our customers from testing an idea to a minimal viable product (MVP) to launching a fully functional product, as well as post-launch considerations. We endeavour to develop a long-term, ongoing relationship with every customer and we believe that your success will drive our success.
                    </p>
                </div>
            </section>
            <br />
            <section class="ws-works-section">
                <div class="container">
                    <div class="row">
                        <div class="ws-works-title">
                            <br />
                            <div class="col-sm-12 ">
                                <h3>Grant Application</h3>
                                <div class="ws-separator"></div>
                            </div>
                            <br />
                            <div class="col-sm-12 text-center clearfix">
                                <div class="row vertical-align">

                                    <p class="text-center">
                                        At Big Brother Apps, we are experienced with working with entrepreneurs.  For entrepreneurs with limited resources, we highly recommend applying for certain government grants that are available to accelerate your business.
                               <br />
                                        <br />
                                        We have successfully obtained government grants in the past and we are happy to assist you with your application.  No matter what stage your business is at, there can be government grants that are suitable for you.  These grants can range anywhere from $1,000 to $100,000, which can significantly lower your financial burdens and allow you to focus on achieving your next milestone.
                                 <br />
                                        <br />
                                        Applying for a grant can be a technical task, as you will need to ensure your answers are viewed favourably according to the grantor’s selection criteria.  Our team is experienced in dealing with government agencies and is more than willing to apply for grants on your behalf. 
                                 <br />
                                        <br />
                                        Our cost can be covered either as a success fee or by you simply agreeing to engage us with the funds received from the grant.  This gives you a peace of mind that we only get paid when you are successful.
                                    </p>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="ws-works-section">
                <div class="container">
                    <div class="row">
                        <div class="ws-works-title">

                            <div class="col-sm-12 ">
                                <h3>Core Team</h3>
                                <div class="ws-separator"></div>
                                <p class="text-center">Our success is an answer of great efforts, diverse technical knowledge and vast experience of our team!</p>
                                <br />
                                <div class="col-sm-3 ws-instagram-item" data-sr='wait 0.05s, ease-in 20px'>
                                    <img src="assets/img/team/MenaTheodorou.jpg" alt="Ciaran" class="img-responsive" />

                                    <div class="ws-works-caption text-center">
                                        <!-- Item Category -->
                                         <h3 class="ws-item-title" style="color: #00843D !important;">Mena Theodorou</h3>
                                        <div class="ws-item-separator"></div>
                                        <div class="ws-item-category" style="color: #000;">Business Development Manager </div>
                                        
                                        <!-- Title -->

                                        
                                        
                                    </div>
                                </div>
                                <div class="col-sm-3 ws-instagram-item" data-sr='wait 0.05s, ease-in 20px'>
                                    <img src="assets/img/team/hana_scott.jpg" alt="Sarah" class="img-responsive" />

                                    <div class="ws-works-caption text-center">

                                        <h3 class="ws-item-title" style="color: #00843D !important;">Hana Scott</h3>
                                        <div class="ws-item-separator"></div>
                                        <div class="ws-item-category" style="color: #000;">Design & QA</div>
                                        <!-- Item Category -->
                                        
                                        <!-- Title -->

                                      
                                        
                                    </div>
                                </div>
                                <div class="col-sm-3 ws-instagram-item" data-sr='wait 0.05s, ease-in 20px'>
                                    <img src="assets/img/team/tingwang.jpg" alt="Ting Wang" class="img-responsive">
                                    <div class="ws-works-caption text-center">
                                        <!-- Item Category -->
                                        <h3 class="ws-item-title" style="color: #00843D !important;">Ting Wang</h3>
                                        <div class="ws-item-separator"></div>
                                        <div class="ws-item-category" style="color: #000;">Business Development Manager</div>
                                        <!-- Title -->

                                        <!-- Price -->
                                        
                                    </div>
                                </div>
                                <!-- Instagram Item -->
                                <div class="col-sm-3 ws-instagram-item" data-sr='wait 0.05s, ease-in 20px'>
                                    <img src="assets/img/team/joe.jpg" alt="JoeZhou" class="img-responsive"/>
                                    <div class="ws-works-caption text-center">
                                        <!-- Item Category -->
                                        <h3 class="ws-item-title" style="color: #00843D !important;">Joe Zhou</h3>
                                        <div class="ws-item-separator"></div>
                                        <div class="ws-item-category" style="color: #000;">Marketing</div>
                                        <!-- Title -->

                                        <!-- Price -->
                                       
                                    </div>
                                </div>
                            </div>
                            <br />

                        </div>
                    </div>
                </div>
            </section>

            <div class="ws-about-content col-sm-12">
                <br />
                <br />

                <h1 class="text-center">Frequently Asked Questions</h1>
                <div class="ws-separator"></div>
                
                <br />
                <h3>How much does it cost?</h3>

                <p class="text-justify">
                    Building a website or mobile application is by no means a one-size-fits-all process.  As such, the cost of doing so can also vary greatly from project to project.
                    <br />

                </p>
                <p class="text-justify">
                    As a ballpark figure, building a mobile application that has a medium level of complexity can cost anywhere between $30,000 to $100,000.  This is a large range primarily because no two applications are identical and the number of modules involved differ from project to project.  Typically, the cost of the project will be determined by the following factors (note the list is not exhaustive):
                </p>
                <br />
                <ul>
                    <li>The overall complexity of the project;</li>
                    <li>Required turnaround time of the project;</li>
                    <li>The number of platforms that the product will be developed on (e.g. iOS only or both iOS and Android);</li>
                    <li>Level of security required (e.g. a digital currency exchange requires extra security and therefore additional cost); and</li>
                    <li>Integrations required (e.g. payment integration) and the difficulty of such integrations.</li>
                </ul>
                <br />
                <p class="text-justify">
                    The good news for you as a business owner or entrepreneur is that you will only need to undertake the project in stages.  We offer milestone payments which normally include a prototype.  You can use the prototype to present to investors or to test the product market fit.  This will limit your risk of investing into a project.
                </p>
                <br />
                <br />
                <br />
                <h3>How long does it take?</h3>
                <p class="text-justify">
                    Based on our experience, if you are releasing a minimum viable product (MVP) to the market, you can expect your application to be delivered within 4 to 6 months.  Having said this, this is dependent on you, as the product owner, communicate effectively and promptly with us.
                    <br />
                    <br />
                    Further, most software development comes with updates or new releases.  The 4 to 6 months time window is only by reference to having a MVP out in the market.  New features and updates will take additional time to implement and are ongoing processes.
                </p>
                <br />
                <br />
                <br />
                <h3>How do I know if my idea will be profitable?</h3>

                <p class="text-justify">
                    There is no guarantee that any idea will be profitable.  However, there are steps you can take to maximise your chance of success.  A few things to note are:
                </p>
                <br />
                <ul>
                    <li>Validate your idea first with the target market.  Is there a real demand for what you are offering?</li>
                    <li>Research your competitors.  Who are the incumbents in the market and what is your point of difference?  If you don’t have a point of difference, is the market large enough to allow more than one player?</li>
                    <li>Understand your market size.</li>
                    <li>Create a business plan.</li>
                    <li>Understand your finances and breakeven point.</li>
                </ul>
                <br />
                <br />
                <br />
                <h3>How do I get more visits / downloads?</h3>

                <p class="text-justify">
                    Visits and downloads come from having a good product, as well as a clear and effective marketing strategy.  There is nothing worse than spending months of your time and money into building something that does not attract any users.
                    There are plenty of resources on the internet in relation to how to create a good customer acquisition strategy.  From Search Engine Optimisation to having a great referral system, there are plenty of levers for entrepreneurs to pull.
                </p>
                <br />
                <p class="text-justify">
                    Bear in mind that marketing involves some test and measure.  If you have a marketing budget, ensure you spend smaller amounts first to test the waters.
                </p>
                <br />
                <br />
                <br />

                <h3>How do I fund my idea?</h3>

                <p class="text-justify">
                    The initial rounds of funding typically comes from FFF (family, friends and founders).  After which, you will need to look for ‘seed funding’.  These typically come from high net worth individuals who like your idea and are willing to back you.
                            <br />
                    <br />
                    When choosing investors, it is important to not only consider who will give you the most money for your equity, but also bear in mind that who will provide the most strategic assistance to your vision.
                            <br />

                </p>

                <br />
                <br />


                <h3>What is the development process?</h3>
                <p class="text-justify">
                    We endeavor to deliver the project on time and on budget.  We will select a team from our existing talent pool that is best suited to your needs.  The typical development process include:
                </p>

                <br />
                <div class="ws-checkout-billing">
                    <ul>
                        <li>Initial workshop to understand and validate your idea;</li>
                        <li>Design of website and / or mobile application wireframes;</li>
                        <li>Development of a prototype for testing;</li>
                        <li>Release of minimum viable product;</li>
                        <li>Ongoing testing and maintenance (cost to be separately agreed).</li>

                    </ul>
                </div>
                <p class="text-justify">
                    We employ an agile methodology and we will constantly seek your feedback to ensure that you are updated and satisfied towards the status of the project.
                </p>


                <!-- Space Helper Class -->


                <br />

            </div>
        </div>
        <br />
</asp:Content>

