﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="BlogDetail.aspx.cs" Inherits="BigBrother.BlogDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>
        <asp:Literal ID="ltTitle" runat="server"></asp:Literal>
    </title>
    <%--<link href="<%= Page.ResolveUrl("~/assets/css/Custom.css") %>" rel="stylesheet" />--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<%= Page.ResolveUrl("~/blog.aspx") %>">Blog</a></li>

                <li class="active">
                    <asp:Literal ID="ltBreadCrumb" runat="server"></asp:Literal></li>
            </ol>
        </div>
    </div>

    <!-- Page Content -->
    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-about-content col-sm-10 col-sm-offset-1">

               
                <div class="row">
                    <div class="col-sm-8">
                      <h3> <asp:Literal ID="ltlBlogTitle" runat="server"></asp:Literal>
                        </h3> 
                        <hr />
                        <i>
                            <asp:Literal ID="ltPublished" runat="server"></asp:Literal></i>
                        <div id="divBlogBody" runat="server">
                        </div>
                    </div>
                      <div class="col-sm-4 ws-checkout-order">
                        <h2>Recent Posts</h2>

                        <ul class="list-group">

                            <asp:Repeater ID="RPRecentPost" runat="server">
                                <ItemTemplate>
                                    <li class="list-group-item">
                                        <a href="<%# Page.GetRouteUrl("Blog",new { title= ToTitle(Eval("Title").ToString()), id=Eval("BlogId") }) %>"><%# Eval("Title") %></a>
                                        <br />
                                        <span class="post-date"><%# string.Format("{0:MMMM dd, yyyy}", Eval("PublishDate")) %></span>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                            


                          

                        </ul>
                           <asp:Literal ID="ltlNoPost" runat="server"></asp:Literal>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>
