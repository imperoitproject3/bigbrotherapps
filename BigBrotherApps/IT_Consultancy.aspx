﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="IT_Consultancy.aspx.cs" Inherits="BigBrother.IT_Consultancy" %>

<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
            <title>Mobile App Development Consultancy</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="assets/img/IT Consultancy.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                </div>
            </div>
        </div>
    </div>
    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-about-content col-sm-12">
                <h1>IT Consultancy</h1>
                <br />
                <p>
                    Are you on a hunt for an outstanding mobile game development company in Ireland or want to become one? You are almost there!<br />
                    We provide free consultancy for mobile app development and can help you realize potential of your concept, select target audience and check app concept and feasibility. 
                </p>
                <br>
                <p>Mobile phones are no longer just simple voice communication devices. With the explosive growth of smartphones, multiple revenue generation engines have come to the fore. Mobile app development is undoubtedly one of the most eminent amongst those. As a result, the app market is flooded with different apps. Thus, one needs to be updated with the latest tools and technology to develop a mobile app and become a leading App. </p>

                <!-- Space Helper Class -->
                <div class="padding-top-x70"></div>


                <div class="row vertical-align">
                    <div class="col-sm-6" data-sr='wait 0.05s, ease-in 20px'>
                        <h3>Start-up Consultancy</h3>
                        <div class="ws-footer-separator"></div>
                        Many Individual and Entrepreneurs in Ireland nurture ambitions about developing mobile  apps. <br /> A good app should be well researched, should include multiple concepts in early stages, should be created artistically, should make your game unique, yet familiar.
                        <br />
                        <br />
                        With our great experience in App Development, we are glad to provide you <span class="bluecolor">  FREE 30 minutes consultancy  </span> in which we understand your concept and based on our experience and expertise provide feedback, suggest changes and help you nurture your concept to full fledge great application. <br />
                         <br />
                        We also give you idea of development cost, time and idea of development procedures and process which will clear all the queries and questions you might have.<br />
                         <br />
                        We also focus on below aspects
                        <br />
                        <ul class="item">
                            <li class="list-group-item">Access to a large audience of all age groups
                       
                            </li>
                            <li class="list-group-item">Huge revenue generating potential
                            </li>
                            <li class="list-group-item">Potential to educate in some way
                            </li>
                            <li class="list-group-item">Capable of becoming part of other industries
                            </li>
                            <li class="list-group-item">Provides branding opportunities for corporate companies

                            </li>

                        </ul>
                        <br />
                        <a class="btn ws-big-btn" href="#ws-register-modal" data-toggle="modal">Let's Talk!</a><Contactform:UserInfoBoxControl runat="server" ID="MyUserInfoBoxControl" />
                    </div>

                    <div class="col-sm-6">
                        <img src="assets/img/Start-up Consultancy.jpg" alt="Alternative Text" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

