﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Portfolio.aspx.cs" Inherits="BigBrother.Portfolio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Mobile App Portfolio</title>
    <style>
        .shadow {
            padding: 5px;
            border: 1px solid #efefef;
            /*box-shadow: 0px 10px 3px;*/
        }

        .HLarge {
            min-height: 95%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<%= Page.ResolveUrl("~/") %>">Home</a></li>
                <li class="active">App Portfolio</li>
            </ol>
        </div>
    </div>
    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-shop-page">


                <!-- Categories Content -->
                <div class="tab-content">
                    <!-- All -->
                    <div role="tabpanel" class="tab-pane fade in active" id="all">
                        <!-- Item -->
                        <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.05s, ease-in 20px'>
                            <div class="shadow">
                                <a href="The_Hurler.aspx">
                                    <div class="ws-item-offer">
                                        <figure>
                                            <img src="assets/img/works/Hurler.jpg" alt="The Hurler" class="img-responsive" />
                                        </figure>
                                    </div>
                                    <div class="ws-works-caption text-center">
                                        <%--<div class="ws-item-category">iOS / Android</div>--%>
                                        <h3 class="ws-item-title">The Hurler</h3>
                                        <div class="ws-item-separator"></div>
                                        <%--<div class="ws-item-price">Case Study</div>--%>
                                    </div>
                                </a>
                            </div>

                        </div>
                        <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.3s, ease-in 20px'>
                            <div class="shadow">
                                <a href="Clubman.aspx">
                                    <figure>
                                        <img src="assets/img/works/Clubman.jpg" alt="Clubman" class="img-responsive" />
                                    </figure>
                                    <div class="ws-works-caption text-center">
                                        <%--<div class="ws-item-category">iOS / Android</div>--%>
                                        <h3 class="ws-item-title">Clubman</h3>
                                        <div class="ws-item-separator"></div>
                                        <%--<div class="ws-item-price">Case Study</div>--%>
                                    </div>
                                </a>
                            </div>

                        </div>
                        <%-- <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.5s, ease-in 20px'>
                    <a href="Extreme-Ireland.aspx">
                        <figure>
                            <img src="assets/img/works/ExtremeIreland.jpg" alt="ExtremeIreland" class="img-responsive" />
                        </figure>
                        <div class="ws-works-caption text-center">
                            <div class="ws-item-category">Android</div>
                            <h3 class="ws-item-title">Extreme Ireland</h3>
                            <div class="ws-item-separator"></div>
                            <div class="ws-item-price">Case Study</div>
                        </div>
                    </a>
                </div>--%>
                        <div class="col-sm-5 col-md-4 ws-works-item" data-sr='wait 0.5s, ease-in 20px'>
                            <div class="shadow">
                                <a href="CoolOff.aspx">
                                    <figure>
                                        <img src="assets/img/works/Cooloff.jpg" alt="Cooloff" class="img-responsive" />
                                    </figure>
                                    <div class="ws-works-caption text-center">
                                        <%--<div class="ws-item-category">iOS / Android</div>--%>
                                        <h3 class="ws-item-title">CoolOff</h3>
                                        <div class="ws-item-separator"></div>
                                        <%--<div class="ws-item-price">Case Study</div>--%>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.05s, ease-in 20px'>
                            <div class="shadow">
                                <a href="Deeds4Kids.aspx">
                                    <figure>
                                        <img src="assets/img/works/Rewardboard.jpg" alt="Deeds4Kids" class="img-responsive" />
                                    </figure>
                                    <div class="ws-works-caption text-center">
                                        <%--<div class="ws-item-category">iOS / Android</div>--%>
                                        <h3 class="ws-item-title">Deeds4Kids</h3>
                                        <div class="ws-item-separator"></div>
                                        <%--<div class="ws-item-price">Case Study</div>--%>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.3s, ease-in 20px'>
                            <div class="shadow">
                                <a href="Audilex.aspx">
                                    <figure>
                                        <img src="assets/img/works/Audilex.jpg" alt="Audilex" class="img-responsive" />
                                    </figure>
                                    <div class="ws-works-caption text-center">
                                        <%--<div class="ws-item-category">iOS / Android</div>--%>
                                        <h3 class="ws-item-title">Audilex</h3>
                                        <div class="ws-item-separator"></div>
                                        <%--<div class="ws-item-price">Case Study</div>--%>
                                    </div>
                                </a>
                            </div>

                        </div>
                        <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.5s, ease-in 20px'>
                            <div class="shadow">
                                <a href="Jiffy_Rides.aspx">
                                    <div class="ws-item-offer">
                                        <figure>
                                            <img src="assets/img/works/Jiffyrides.jpg" alt="JiffyRides" class="img-responsive" />
                                        </figure>
                                    </div>
                                    <div class="ws-works-caption text-center">
                                        <%--<div class="ws-item-category">iOS / Android</div>--%>
                                        <h3 class="ws-item-title">Jiffy Rides</h3>
                                        <div class="ws-item-separator"></div>
                                        <%--<div class="ws-item-price">Case Study</div>--%>
                                    </div>
                                </a>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />


</asp:Content>
