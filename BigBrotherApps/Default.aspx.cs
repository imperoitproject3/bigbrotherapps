﻿using BigBrother.Models;
using BigBrother.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigBrother
{
    public partial class Default1 : System.Web.UI.Page
    {
        public static string ToTitle(string value)
        {
            return value.Replace(" ", "-").ToLower();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindBlogs();
            }
        }

        private void BindBlogs()
        {
            //commit for verification
            //BigBrotherEntities _ctx = new BigBrotherEntities();
            //List<BlogMasterModel> blogs = (from b in _ctx.BlogMaster
            //                               select new BlogMasterModel
            //                               {
            //                                   BlogId = b.BlogId,
            //                                   Title = b.Title,
            //                                   Tag = b.Tag,
            //                                   Author = b.Author,
            //                                   ImageUrl = b.ImageUrl,
            //                                   PublishDate = b.PublishDate,
            //                                   BlogBody=b.BlogBody
            //                               }).Take(3).ToList();

            //if (blogs != null && blogs.Any())
            //{
            //    rptBlogs.DataSource = blogs;
            //}
            //else
            //    rptBlogs.DataSource = null;
            //rptBlogs.DataBind();
        }
    }
}