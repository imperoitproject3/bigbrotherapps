﻿using BigBrother.DAL;
using System;
using System.IO;
using System.Linq;

namespace BigBrother.Admin
{
    public partial class addblog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["AdminID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BigBrotherEntities _ctx = new BigBrotherEntities();

            BlogMaster data = _ctx.BlogMaster.FirstOrDefault(x => x.Title.ToLower() == txtTitle.Text.ToString().ToLower());
            if (data != null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "DuplicateBlog", "fnDuplicateBlog()", true);
            }
            else
            {
                if (fileImage != null && fileImage.HasFile)
                {
                    BlogMaster objBlog = new BlogMaster
                    {
                        Title = txtTitle.Text.ToString(),
                        Author = txtAuthor.Text,
                        PublishDate = DateTime.Now,
                        ImageUrl = "A",
                        BlogBody = txtDescription.Text,
                        Tag = txtTag.Text
                    };

                    string ogFileName = fileImage.FileName.Trim('\"');
                    string shortGuid = Guid.NewGuid().ToString();
                    var thisFileName = shortGuid + Path.GetExtension(ogFileName);

                    string BlogImagesPath = Server.MapPath("~/Files/BlogImages/");
                    fileImage.SaveAs(Path.Combine(BlogImagesPath, thisFileName));
                    objBlog.ImageUrl = thisFileName;
                    _ctx.BlogMaster.Add(objBlog);
                    _ctx.SaveChanges();
                }
                Response.Redirect("blogs.aspx");
            }
        }
    }
}