﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/BigBrotherAdmin.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BigBrother.Admin.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#liHome').addClass('active');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 border-right-blue-grey border-right-lighten-5">
                                <div class="p-1 text-xs-center">
                                    <div>
                                        <a href="inquiries.aspx">
                                            <h3 class="font-large-1 success darken-1 text-bold-400">
                                                <asp:Label runat="server" ID="lnktotalinquiry"></asp:Label>
                                            </h3>
                                            <span class="font-medium-2 grey darken-1">Total Inquiry</span>
                                        </a>
                                    </div>
                                    <hr />
                                    <div class="card-body overflow-hidden">
                                        <ul class="list-inline clearfix mb-0">
                                            <li class="col-md-3 border-right-grey border-right-lighten-2 pr-2">
                                                <a href="inquiries.aspx?Status=Closed (Negative)">
                                                    <h3 class="success text-bold-400">
                                                        <asp:Label runat="server" ID="lnkNegativeClosed"></asp:Label>
                                                    </h3>
                                                    <span class="font-medium-1 grey darken-1">Closed (Negative)</span>
                                                </a>
                                            </li>
                                            <li class="col-md-3 border-right-grey border-right-lighten-2 pr-2">
                                                <a href="inquiries.aspx?Status=Positive">
                                                    <h3 class="success text-bold-400">
                                                        <asp:Label runat="server" ID="lnkpositive"></asp:Label>
                                                    </h3>
                                                    <span class="font-medium-1 grey darken-1">Positive</span>
                                                </a>
                                            </li>
                                            <li class="col-md-3">
                                                <a href="inquiries.aspx?Status=successfully Closed">
                                                    <h3 class="success text-bold-400">
                                                        <asp:Label runat="server" ID="lnksuccessfullyClosed"></asp:Label></h3>
                                                    <span class="font-medium-1 grey darken-1">successfully Closed</span>
                                                </a>
                                            </li>
                                            <li class="col-md-3">
                                                <a href="inquiries.aspx?Status=In Discussion">
                                                    <h3 class="success text-bold-400">
                                                        <asp:Label runat="server" ID="lnkDisscuss"></asp:Label></h3>
                                                    <span class="font-medium-1 grey darken-1">In Discussion</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
