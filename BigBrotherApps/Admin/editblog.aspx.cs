﻿using BigBrother.DAL;
using System;
using System.IO;
using System.Linq;
using System.Web.UI;

namespace BigBrother.Admin
{
    public partial class editblog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["AdminID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                {
                    if (Request.QueryString["BlogId"] != null)
                    {
                        int BlogId = Convert.ToInt32(Request.QueryString["BlogId"].ToString());
                        BindBlog(BlogId);
                    }
                    else
                        Response.Redirect("blogs.aspx");
                }
            }
        }

        private void BindBlog(int BlogId)
        {
            BigBrotherEntities _ctx = new BigBrotherEntities();
            BlogMaster data = _ctx.BlogMaster.FirstOrDefault(x => x.BlogId == BlogId);
            if (data == null)
            {
                Response.Redirect("blogs.aspx");
            }
            else
            {
                txtTitle.Text = data.Title;
                txtAuthor.Text = data.Author;
                txtDescription.Text = data.BlogBody;
                txtTag.Text = data.Tag;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int BlogId = Convert.ToInt32(Request.QueryString["BlogId"].ToString());
            BigBrotherEntities _ctx = new BigBrotherEntities();

            if (_ctx.BlogMaster.Any(x => x.Title.ToLower() == txtTitle.Text.ToString().ToLower() && x.BlogId != BlogId))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "DuplicateBlog", "fnDuplicateBlog()", true);
            }
            else
            {
                BlogMaster data = _ctx.BlogMaster.FirstOrDefault(x => x.BlogId == BlogId);

                if (data != null)
                {
                    if (fileImage != null && fileImage.HasFile)
                    {
                        string ogFileName = fileImage.FileName.Trim('\"');
                        string shortGuid = Guid.NewGuid().ToString();
                        var thisFileName = BlogId + "_" + shortGuid + Path.GetExtension(ogFileName);

                        string BlogImagesPath = Server.MapPath("~/Files/BlogImages/");
                        fileImage.SaveAs(Path.Combine(BlogImagesPath, thisFileName));
                        data.ImageUrl = thisFileName;
                    }

                    data.Title = txtTitle.Text;
                    data.Author = txtAuthor.Text;
                    data.BlogBody = txtDescription.Text;
                    data.Tag = txtTag.Text;
                    _ctx.SaveChanges();
                }
                Response.Redirect("blogs.aspx");
            }
        }
    }
}