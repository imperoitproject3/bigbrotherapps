﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/BigBrotherAdmin.Master" AutoEventWireup="true" CodeBehind="inquiries.aspx.cs" Inherits="BigBrother.Admin.inquiries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".jsgrid-grid-body").css('overflow-x', 'auto');
            $('#liinquiries').addClass('active');
        });
        $(".deleteData").click(function () {
            deleteRow($(this), 'fsdfsd');
        });
    </script>
    <style>
        .red {
            color: red;
        }

        .green {
            color: green;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="basic">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">All Inquiries</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li>
                                    <a href="<%= Page.ResolveUrl("addenquiry.aspx") %>">
                                        <i class="icon-plus3"></i>Add New Inquiry
                                    </a>
                                </li>
                                <li>
                                    <asp:LinkButton runat="server" ID="lnkexport" CssClass="btn btn-info btn-rounded" OnClick="lnkexport_Click">
                                         <i class="fa fa-file-excel-o"></i>Export To Excel
                                    </asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <div id="basicScenario" class="jsgrid" style="position: relative; height: auto; width: 100%;">
                                <div class="jsgrid-grid-body">
                                    <table class="Datatable table table-hover table-striped" style="margin-top: 0px;">
                                        <thead style="display: table-header-group;">
                                            <tr>
                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner">Name & Status</div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner ">Email & Number</div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner ">Address</div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                <%--  <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner ">Number</div>
                                                    <div class="fht-cell"></div>
                                                </th>--%>
                                                <%-- <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner ">Status</div>
                                                    <div class="fht-cell"></div>
                                                </th>--%>

                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner ">Date</div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner ">Message</div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner ">Action</div>
                                                    <div class="fht-cell"></div>
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater runat="server" ID="GV" OnItemCommand="GV_ItemCommand">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("Name")%>
                                                            <br />
                                                            <span class="<%# Eval("color")%>"><%# Eval("Status")%></span></td>
                                                        <td style="word-wrap: break-word; width: 10px"><%# Eval("EmailAddress")%><br />
                                                            <%# Eval("Number")%>
                                                        </td>
                                                        <td><%# Eval("Address")%></td>
                                                        <td><%# Eval("Date")%></td>
                                                        <td><%# Eval("Message")%></td>
                                                        
                                                        <td>
                                                            <a class="btn btn-info btn-sm" href="editenquiry.aspx?id=<%# Eval("InquiryID")%>">Edit
                                                            </a>
                                                          
                                                            <asp:LinkButton runat="server" class="btn btn-sm btn-danger m_top_5" CommandArgument='<%# Eval("InquiryID")%>' CommandName="delete"
                                                                OnClientClick="return confirm('Are you sure you want delete this Inquiry');">Delete
                                                            </asp:LinkButton>

                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
