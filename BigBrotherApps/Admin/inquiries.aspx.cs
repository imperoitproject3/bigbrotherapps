﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using BigBrother.DAL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigBrother.Admin
{
    public partial class inquiries : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["AdminID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else if (Request.QueryString["Status"] != null)
                {
                    var Status = Request.QueryString["Status"].ToString();
                    BindGrid(Status);
                }
                else
                    BindGrid();
            }
        }

        private void BindGrid()
        {
            BigBrotherEntities _ctx = new BigBrotherEntities();
            //var enquiries = _ctx.Inquiry.OrderByDescending(x => x.InquiryID).ThenBy(x => x.Status == "Positive").ToList();

            var enquiries = _ctx.Inquiry.OrderByDescending(x => x.InquiryID).ToList();

            var test = enquiries.Select(x => new InquiryView
            {
                Address = x.Address,
                Attachment = x.Attachment,
                Date = x.Date,
                InquiryID = x.InquiryID,
                EmailAddress = x.EmailAddress,
                IsActive = x.IsActive,
                Message = x.Message,
                Name = x.Name,
                Number = x.Number,
                Reason = x.Reason,
                Remark = x.Remark,
                Status = x.Status != "None" ? x.Status : string.Empty,
                Color = x.Status == "Successfully Closed" ? "green" : (x.Status == "Closed (Negative)" ? "red" : "Black")
            }).ToList();
            GV.DataSource = test;
            GV.DataBind();
        }

        private void BindGrid(string Status)
        {
            BigBrotherEntities _ctx = new BigBrotherEntities();
            var enquiries = _ctx.Inquiry.Where(x => x.Status.Contains(Status)).OrderByDescending(x => x.InquiryID).ThenBy(x => x.Status == "Positive").ToList();

            var test = enquiries.Select(x => new InquiryView
            {
                Address = x.Address,
                Attachment = x.Attachment,
                Date = x.Date,
                InquiryID = x.InquiryID,
                EmailAddress = x.EmailAddress,
                IsActive = x.IsActive,
                Message = x.Message,
                Name = x.Name,
                Number = x.Number,
                Reason = x.Reason,
                Remark = x.Remark,
                Status = x.Status != "None" ? x.Status : string.Empty,
                Color = x.Status == "Successfully Closed" ? "green" : (x.Status == "Closed (Negative)" ? "red" : "Black")
            }).ToList();
            GV.DataSource = test;
            GV.DataBind();
        }

        protected void GV_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete" && e.CommandArgument != null)
            {
                int InquiryId = Convert.ToInt32(e.CommandArgument);
                if (InquiryId > 0)
                {
                    BigBrotherEntities _ctx = new BigBrotherEntities();
                    Inquiry objDelete = _ctx.Inquiry.FirstOrDefault(x => x.InquiryID == InquiryId);
                    if (objDelete != null)
                    {
                        _ctx.Inquiry.Remove(objDelete);
                        _ctx.SaveChanges();
                        BindGrid();
                    }
                }
            }
        }

        public partial class InquiryView
        {
            public int InquiryID { get; set; }
            public string Name { get; set; }
            public string EmailAddress { get; set; }
            public string Address { get; set; }
            public string Number { get; set; }
            public string Message { get; set; }
            public bool IsActive { get; set; }
            public string Date { get; set; }
            public string Attachment { get; set; }
            public string Remark { get; set; }
            public string Status { get; set; }
            public string Reason { get; set; }
            public string Color { get; set; }
        }

        private void ExportToExcelSheet()
        {
            BigBrotherEntities _ctx = new BigBrotherEntities();
            var result = _ctx.Inquiry.OrderByDescending(x => x.InquiryID).ThenBy(x => x.Status == "Positive").ToList();
            DateTime nowDate = DateTime.UtcNow;
            string fileName = "Inquiry" + nowDate.ToString("MMMddyyyy") + ".xlsx";
            bool isexists = Directory.Exists(Server.MapPath("~/" + "assets" + "/" + "ExcelReport"));
            if (isexists == false)
                Directory.CreateDirectory(Server.MapPath("~/" + "assets" + "/" + "ExcelReport/"));
            string outputDir = Server.MapPath("~/" + "assets" + "/" + "ExcelReport/");

            FileInfo file = new FileInfo(outputDir + fileName);

            if (file.Exists)
                file.Delete();
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Total Survey List" + DateTime.Now.ToShortDateString());
                int RowIndex = 1;
                ExcelRange Range = Range = worksheet.Cells[RowIndex, 1, RowIndex, 6];

                worksheet.Row(RowIndex).Height = 50;
                worksheet.Row(RowIndex).Style.Fill.PatternType = ExcelFillStyle.Solid;
                Range.Style.Fill.BackgroundColor.SetColor(Color.DarkRed);
                Range.Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                Range.Style.Border.Bottom.Color.SetColor(Color.Black);
                worksheet.Row(RowIndex).Style.Font.Color.SetColor(Color.White);
                worksheet.Row(RowIndex).Style.Font.Size = 10;
                worksheet.Row(RowIndex).Style.Font.Bold = true;
                worksheet.Row(RowIndex).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(RowIndex).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Row(RowIndex).Height = 20;
                Range.Merge = true;
                Range.Value = "All Inquiry";
                RowIndex += 2;

                worksheet.Cells[RowIndex, 4, RowIndex + 2, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[RowIndex, 4, RowIndex + 2, 6].Style.Border.Bottom.Color.SetColor(Color.Black);


                worksheet.Cells[RowIndex, 1].Merge = true;
                worksheet.Cells[RowIndex, 1].Style.Font.Bold = true;
                worksheet.Cells[RowIndex, 1].Value = "Name";

                worksheet.Cells[RowIndex, 2].Merge = true;
                worksheet.Cells[RowIndex, 2].Style.Font.Bold = true;
                worksheet.Cells[RowIndex, 2].Value = "Email";

                worksheet.Cells[RowIndex, 3].Merge = true;
                worksheet.Cells[RowIndex, 3].Style.Font.Bold = true;
                worksheet.Cells[RowIndex, 3].Value = "Status";

                worksheet.Cells[RowIndex, 4].Merge = true;
                worksheet.Cells[RowIndex, 4].Style.Font.Bold = true;
                worksheet.Cells[RowIndex, 4].Value = "Address";

                worksheet.Cells[RowIndex, 5].Merge = true;
                worksheet.Cells[RowIndex, 5].Style.Font.Bold = true;
                worksheet.Cells[RowIndex, 5].Value = "Number";

                worksheet.Cells[RowIndex, 6].Merge = true;
                worksheet.Cells[RowIndex, 6].Style.Font.Bold = true;
                worksheet.Cells[RowIndex, 6].Value = "Date";

                RowIndex++;
                int columnIndex = 1;
                foreach (var objData in result)
                {
                    worksheet.Cells[RowIndex, columnIndex + 0].Value = objData.Name;

                    worksheet.Cells[RowIndex, columnIndex + 1].AutoFitColumns();
                    worksheet.Cells[RowIndex, columnIndex + 1].Value = objData.EmailAddress;

                    worksheet.Cells[RowIndex, columnIndex + 2].Value = objData.Status;

                    worksheet.Cells[RowIndex, columnIndex + 3].Value = objData.Address;

                    worksheet.Cells[RowIndex, columnIndex + 4].Value = objData.Number;

                    worksheet.Cells[RowIndex, columnIndex + 5].Value = objData.Date;

                    RowIndex++;
                }
                worksheet.Column(1).AutoFit();
                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();
                worksheet.Column(4).AutoFit();
                worksheet.Column(5).AutoFit();
                worksheet.Column(6).AutoFit();
                worksheet.Column(7).AutoFit();
                package.Save();
            }
            Response.Redirect("~/assets/ExcelReport/" + fileName);
        }

        protected void lnkexport_Click(object sender, EventArgs e)
        {
            ExportToExcelSheet();
        }
    }
}