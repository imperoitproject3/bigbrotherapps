﻿using BigBrother.DAL;
using System;

namespace BigBrother.Admin
{
    public partial class addenquiry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["AdminID"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BigBrotherEntities _ctx = new BigBrotherEntities();
            Inquiry data = new Inquiry
            {
                Name = txtName.Text,
                EmailAddress = txtEmail.Text,
                Address = txtAddress.Text,
                Number = txtnumber.Text,
                Message = txtMessage.Text,
                Date = DateTime.Now.ToString("dd/MM/yyyy"),
                Remark = txtRemark.Text,
                Status = ddlStatus.SelectedValue,
                Reason = txtReason.Text,
            };
            _ctx.Inquiry.Add(data);
            _ctx.SaveChanges();
            Response.Redirect("inquiries.aspx");
        }
    }
}