﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/BigBrotherAdmin.Master" AutoEventWireup="true" CodeBehind="addenquiry.aspx.cs" Inherits="BigBrother.Admin.addenquiry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function resizeTextBox(txt) {
            txt.style.height = "4px";
            txt.style.height = (1 + txt.scrollHeight) + "px";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="validation">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add Inquiry</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-md-2">Name <span class="red">*</span></label>
                                        <div class="col-md-5">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                                             <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtName" ErrorMessage="Please enter name"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Email<span class="red">*</span></label>
                                        <div class="col-md-5">
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                              <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter email"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Address<span class="red">*</span></label>
                                        <div class="col-md-5">
                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control"></asp:TextBox>
                                               <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtAddress" ErrorMessage="Please enter address"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Number <span class="red">*</span></label>
                                        <div class="col-md-5">
                                            <asp:TextBox ID="txtnumber" runat="server" CssClass="form-control" ></asp:TextBox>
                                                <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtnumber" ErrorMessage="Please enter number"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                   

                                    <div class="form-group row" id="divAttachment" runat="server">
                                        <label class="col-md-2">Attachment</label>
                                        <div class="col-md-5">
                                            <asp:HyperLink ID="lnkAttachment" runat="server"></asp:HyperLink>
                                        </div>
                                    </div>

                                      <div class="form-group row">
                                        <label class="col-md-2">Status</label>
                                        <div class="col-md-5">
                                            <asp:DropDownList ID="ddlStatus" CssClass="form-control required" runat="server">
                                                <asp:ListItem Value="None" Text="Select "></asp:ListItem>
                                                <asp:ListItem Value="Positive " Text="Positive "></asp:ListItem>
                                                <asp:ListItem Value="Closed (Negative)" Text="Closed (Negative)"></asp:ListItem>
                                                <asp:ListItem Value="Successfully Closed" Text="Successfully Closed"></asp:ListItem>
                                                <asp:ListItem Value="In Discussion" Text="In Discussion"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <label class="col-md-2">Message<span class="red">*</span></label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtMessage" runat="server" CssClass="form-control"  TextMode="MultiLine"  onkeyup="resizeTextBox(this)"></asp:TextBox>
                                                  <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtMessage" ErrorMessage="Please enter message"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Remark <span class="red">*</span> </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" CssClass="form-control" MaxLength="1000"></asp:TextBox>
                                                  <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtRemark" ErrorMessage="Please enter remark"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2">Reason <span class="red">*</span></label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" CssClass="form-control" MaxLength="1000"></asp:TextBox>
                                                 <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtReason" ErrorMessage="Please enter reason"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn btn-md btn-primary" />
                                            <a href="<%= Page.ResolveUrl("inquiries.aspx") %>" class="btn btn-md btn-grey">Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
