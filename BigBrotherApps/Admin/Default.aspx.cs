﻿using BigBrother.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigBrother.Admin
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["AdminID"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                else
                    BindDashboard();
            }
        }
        private void BindDashboard()
        {
            BigBrotherEntities _ctx = new BigBrotherEntities();
            var TotalInquiry = _ctx.Inquiry.ToList();
            lnktotalinquiry.Text = TotalInquiry.Count().ToString();
            lnkNegativeClosed.Text = TotalInquiry.Count(x => x.Status == "Closed (Negative)").ToString();
            lnkDisscuss.Text = TotalInquiry.Count(x => x.Status == "In Discussion").ToString();
            lnksuccessfullyClosed.Text = TotalInquiry.Count(x => x.Status == "Successfully Closed").ToString();
            lnkpositive.Text = TotalInquiry.Count(x => x.Status == "Positive ").ToString();
        }
    }
}