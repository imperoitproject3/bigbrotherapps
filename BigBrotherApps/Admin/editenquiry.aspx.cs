﻿using BigBrother.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigBrother.Admin
{
    public partial class editenquiry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int InquiryId = Convert.ToInt32(Request.QueryString["id"].ToString());
                    BindEditEnquiry(InquiryId);
                }
                else
                    Response.Redirect("inquiries.aspx");
            }
        }

        private void BindEditEnquiry(int InquiryId)
        {
            BigBrotherEntities _ctx = new BigBrotherEntities();
            Inquiry data = _ctx.Inquiry.FirstOrDefault(x => x.InquiryID == InquiryId);
            if (data == null)
            {
                Response.Redirect("inquiries.aspx");
            }
            else
            {
                txtName.Text = data.Name;
                txtEmail.Text = data.EmailAddress;
                txtAddress.Text = data.Address;
                txtMessage.Text = data.Message;
                txtnumber.Text = data.Number;
                ddlStatus.SelectedValue = data.Status;
                txtReason.Text = data.Reason;
                if (!string.IsNullOrEmpty(data.Attachment))
                {
                    lnkAttachment.NavigateUrl = Page.ResolveUrl("~/Attachment/" + data.Attachment);
                    lnkAttachment.Text = "Click Here";
                    divAttachment.Style.Add("display", "block;");
                }
                else
                    divAttachment.Style.Add("display", "none;");
                txtRemark.Text = data.Remark;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int InquiryId = Convert.ToInt32(Request.QueryString["id"].ToString());
            BigBrotherEntities _ctx = new BigBrotherEntities();
            Inquiry data = _ctx.Inquiry.FirstOrDefault(x => x.InquiryID == InquiryId);
            if (data == null)
            {
                Response.Redirect("inquiries.aspx");
            }
            else
            {
                data.Remark = txtRemark.Text;
                data.Status = ddlStatus.SelectedValue;
                data.Reason = txtReason.Text;
                _ctx.SaveChanges();
                Response.Redirect("inquiries.aspx");
            }
        }
    }
}