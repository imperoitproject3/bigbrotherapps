﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/BigBrotherAdmin.Master" AutoEventWireup="true" CodeBehind="editenquiry.aspx.cs" Inherits="BigBrother.Admin.editenquiry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="validation">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Enquiry</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-md-2">Name  </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Email  </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Address   </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Number   </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtnumber" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>

                                  

                                    <div class="form-group row" id="divAttachment" runat="server">
                                        <label class="col-md-2">Attachment  </label>
                                        <div class="col-md-6">
                                            <asp:HyperLink ID="lnkAttachment" runat="server"></asp:HyperLink>
                                        </div>
                                    </div>
                                      <div class="form-group row">
                                        <label class="col-md-2">Status</label>
                                        <div class="col-md-6">
                                            <asp:DropDownList ID="ddlStatus" CssClass="form-control required" runat="server">
                                                <asp:ListItem Value="None" Text="Select "></asp:ListItem>
                                                <asp:ListItem Value="Positive " Text="Positive "></asp:ListItem>
                                                <asp:ListItem Value="Closed (Negative)" Text="Closed (Negative)"></asp:ListItem>
                                                <asp:ListItem Value="Successfully Closed" Text="Successfully Closed"></asp:ListItem>
                                                <asp:ListItem Value="In Discussion" Text="In Discussion"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                      <div class="form-group row">
                                        <label class="col-md-2">Message   </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtMessage" runat="server" CssClass="form-control" Enabled="false" TextMode="MultiLine" Rows="8" Columns="0"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-2">Remark </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" TextMode="MultiLine"  MaxLength="1000"></asp:TextBox>
                                        </div>
                                    </div>
                                  
                                    <div class="form-group row">
                                        <label class="col-md-2">Reason   </label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtReason" TextMode="MultiLine" runat="server" CssClass="form-control" MaxLength="1000"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn btn-md btn-primary" />
                                            <a href="<%= Page.ResolveUrl("inquiries.aspx") %>" class="btn btn-md btn-grey">Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
