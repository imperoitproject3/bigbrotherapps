﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Referral_program.aspx.cs" Inherits="BigBrother.Referral_Program" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Referral Big Brother Apps</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Individual").hide();
            $("#ITcompany").show();
        });
        function TextValueChange(drdRefer) {
            var select_item = drdRefer.value;
            if (select_item == '1') {
                $("#Individual").hide();
                $("#ITcompany").show();
                $('#lblName').text("Company Name ");
            }
            else if (select_item == '2') {
                $("#Individual").show();
                $("#ITcompany").hide();
                $('#lblName').text("Your Name ");
            } else {
                $("#Individual").hide();
                $("#ITcompany").hide();
                $('#lblName').text("Your Name ");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="/assets/img/Referralheader.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>Referral Program</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Parallax Header -->

    <!-- Page Content -->
    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-contact-page">

                <!-- General Information -->
                <div class="col-sm-6">
                    <div class="ws-contact-info">
                        <h2>Looking for a great App company to partner with and grow?</h2>
                        <p>
                            Let’s be blunt! Our referrals are our primary source of business.
                            We continue to do well because we are frequently recommended.
                            Recommend us and get rewarded.
                        </p>
                        <br />
                        <div id="ITcompany">
                            <h2>For IT company</h2>
                            <p>If you love Square Root as much as we do, we’d appreciate your referral! You can refer entrepreneurs just like yourself via filling in Referral form! As an added incentive for doing so we will offer YOU up 20% from the cost of each project completed.Thanks for spreading the word!</p>
                        </div>

                        <div id="Individual">
                            <h2>Individual</h2>
                            <p>If you love Square Root as much as we do, we’d appreciate your referral! You can refer entrepreneurs just like yourself via phone, email, social media or both! We’ll even give everyone who enquires via email a discount voucher to save €2000 Thanks for spreading the word!</p>
                        </div>

                    </div>
                </div>

                <!-- Contact Form -->
                <div class="col-sm-6">
                    <div class="form-horizontal ws-contact-form">
                        <div class="form-group">
                            <asp:Label ID="spanMessage" Visible="false" CssClass="alert alert-success contactformmsg" runat="server"></asp:Label>
                        </div>
                        <div class="form-group">
                            <label id="lblReferral" class="control-label">You are <span>*</span></label>
                            <asp:DropDownList ID="drdRefer" runat="server" class="form-control dropdown" onchange="TextValueChange(this)">
                                <asp:ListItem Value="1" Text="IT company "></asp:ListItem>
                                <asp:ListItem Value="2" Text="Individual "></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label id="lblName" class="control-label">Your Name</label><label class="control-label"><span> *</span></label>
                            <asp:TextBox ID="txtName" MaxLength="50" class="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="ContactForm1" ID="rqv" CssClass="red"
                                ErrorMessage="Please enter name" Display="Dynamic" runat="server" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                        </div>
                        <!-- Email -->
                        <div class="form-group">
                            <label class="control-label">Email <span>*</span></label>
                            <asp:TextBox ID="txtEmail" MaxLength="50" class="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="ContactForm1" Display="Dynamic" CssClass="red" ID="RequiredFieldValidator3"
                                runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter email"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator Display="Dynamic" CssClass="red" ID="RegularExpressionValidator1" runat="server"
                                ControlToValidate="txtEmail" ValidationGroup="ContactForm1" ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                                ErrorMessage="Email address invalid"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Phone <span>*</span></label>
                            <asp:TextBox ID="txtContactNumber" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator CssClass="red" ID="RequiredFieldValidator1" ControlToValidate="txtContactNumber"
                                runat="server" Display="Dynamic" ValidationGroup="ContactForm1" ErrorMessage="Please enter phone number"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator Display="Dynamic" CssClass="red" ValidationGroup="ContactForm1" ValidationExpression="\d+"
                                ControlToValidate="txtContactNumber" ID="RegularExpressionValidator3" runat="server"
                                ErrorMessage="Phone Number Invalid"></asp:RegularExpressionValidator>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Location</label>
                            <asp:TextBox ID="txtlocation" MaxLength="50" class="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Remark </label>
                            <asp:TextBox ID="txtRemark" MaxLength="50" class="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <h4>Referral Info</h4>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Name <span>*</span></label>
                            <asp:TextBox ID="txtReferralName" MaxLength="50" class="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="ContactForm1" ID="RequiredFieldValidator2" CssClass="red"
                                ErrorMessage="Please enter the referal's name" Display="Dynamic" runat="server" ControlToValidate="txtReferralName"></asp:RequiredFieldValidator>
                        </div>
                        <!-- Email -->
                        <div class="form-group">
                            <label class="control-label">Email <span>*</span></label>
                            <asp:TextBox ID="txtReferralEmail" MaxLength="50" class="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ValidationGroup="ContactForm1" Display="Dynamic" CssClass="red" ID="RequiredFieldValidator4"
                                runat="server" ControlToValidate="txtReferralEmail" ErrorMessage="Please enter the referal's email"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator Display="Dynamic" CssClass="red" ID="RegularExpressionValidator2" runat="server"
                                ControlToValidate="txtReferralEmail" ValidationGroup="ContactForm1" ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                                ErrorMessage="Referral email address invalid"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Phone <span>*</span></label>
                            <asp:TextBox ID="txtReferralPhone" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator CssClass="red" ID="RequiredFieldValidator5" ControlToValidate="txtReferralPhone"
                                runat="server" Display="Dynamic" ValidationGroup="ContactForm1" ErrorMessage="Please enter the referal's phone number"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator Display="Dynamic" CssClass="red" ValidationGroup="ContactForm1" ValidationExpression="\d+"
                                ControlToValidate="txtReferralPhone" ID="RegularExpressionValidator4" runat="server"
                                ErrorMessage="Referral phone number invalid"></asp:RegularExpressionValidator>
                        </div>
                        <!-- Submit Button -->
                        <div class="form-group">
                            <asp:Button ID="btnSubmit" ValidationGroup="ContactForm1" CssClass="btn ws-big-btn"
                                Text="Submit" runat="server" OnClick="btnSubmit_Click" />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
