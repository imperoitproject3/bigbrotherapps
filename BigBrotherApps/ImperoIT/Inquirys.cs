﻿using System.Data;
using System.Data.SqlClient;

namespace ImperoIT
{
    internal class Inquirys
    {
        #region Public Variables

        public int InquiryID { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string Number { get; set; }
        public string Address { get; set; }
        public string Message { get; set; }
        public string Attachment { get; set; }
        public string Remark { get; set; }
        const string sSql = "SP_Inquiry";

        #endregion

        #region Private Functions

        public int AddInquiry()
        {
            SqlParameter[] p = new SqlParameter[8];
            p[0] = new SqlParameter("@Name", Name);
            p[1] = new SqlParameter("@Number", Number);
            p[2] = new SqlParameter("@EmailAddress", EmailAddress);
            p[3] = new SqlParameter("@Address", Address);
            p[4] = new SqlParameter("@Attachment", Attachment); // Attachment
            p[5] = new SqlParameter("@Remark", Remark);
            p[6] = new SqlParameter("@Message", Message);
            p[7] = new SqlParameter("@Mode", "A"); // Add Inquiry
            return ImperoIT.SqlHelper.ExecuteNonQuery(new SqlConnection(SqlHelper.GetConnectionString()), CommandType.StoredProcedure, sSql, p);
        }
        public int UpdateInquiry()
        {
            SqlParameter[] p = new SqlParameter[8];
            p[0] = new SqlParameter("@InquiryID", InquiryID);
            p[1] = new SqlParameter("@Name", Name);
            p[2] = new SqlParameter("@Number", Number);
            p[3] = new SqlParameter("@EmailAddress", EmailAddress);
            p[4] = new SqlParameter("@Address", Address);
            p[5] = new SqlParameter("@Message", Message);
            p[6] = new SqlParameter("@Remark", Remark);
            p[7] = new SqlParameter("@Mode", "U"); // Update Inquiry
            return ImperoIT.SqlHelper.ExecuteNonQuery(new SqlConnection(SqlHelper.GetConnectionString()), CommandType.StoredProcedure, sSql, p);
        }

        public DataSet GetAllInquiry()
        {
            SqlParameter[] p = new SqlParameter[2];
            p[0] = new SqlParameter("@Mode", "S"); // Select All Or One Inquiry
            p[1] = new SqlParameter("@InquiryID", "0");
            return ImperoIT.SqlHelper.ExecuteDataset(new SqlConnection(SqlHelper.GetConnectionString()), CommandType.StoredProcedure, sSql, p);
        }

        public DataSet GetInquiryByID()
        {
            SqlParameter[] p = new SqlParameter[2];
            p[0] = new SqlParameter("@Mode", "S"); // Select All Or One Inquiry
            p[1] = new SqlParameter("@InquiryID", InquiryID);
            return ImperoIT.SqlHelper.ExecuteDataset(new SqlConnection(SqlHelper.GetConnectionString()), CommandType.StoredProcedure, sSql, p);
        }

        public int DeleteInquiry()
        {
            SqlParameter[] p = new SqlParameter[2];
            p[0] = new SqlParameter("@InquiryID", InquiryID);
            p[1] = new SqlParameter("@Mode", "D");
            return ImperoIT.SqlHelper.ExecuteNonQuery(new SqlConnection(SqlHelper.GetConnectionString()), CommandType.StoredProcedure, sSql, p);
        }

        #endregion
    }
}