﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BigBrother.Default1" %>

<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="canonical" href="https://BigBrother.ie/" />
    <title>Mobile App Solutions and IT services company in Brisbane, Australia</title>
    <style>
        .shadow {
            padding: 5px;
            border: 1px solid #efefef;
            /*box-shadow: 0px 10px 3px;*/
        }
        .HLarge {
            height: 371px;
            /*min-height: 95%;*/
        }

        @media only screen and (max-width: 600px) {
            .HLarge {
                height: auto;
            }
        }

        @media (max-width: 900px) {
            #slide-699-layer-1 {
                display: none;
            }

            #slide-699-layer-8 {
                display: none;
            }

            .forcefullwidth_wrapper_tp_banner {
                height: 200px !important;
            }

            .testClass {
                background-image: url('assets/img/backgrounds/BackgroundMobile.png') !important;
                background-repeat: no-repeat !important;
                background-size: cover !important;
                background-position: center center !important;
                background-color: rgb(245, 245, 245) !important;
                margin: 0px auto !important;
                padding: 0px !important;
                position: absolute !important;
                overflow: visible !important;
                height: 200px !important;
                width: 100% !important;
                left: 0px !important;
            }
        }
    </style>
    <meta name="description" content="Big brother apps, is leading software company which provide top custom mobile app development services in Brisbane, Australia." />
    <%--<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script>
    <script type="text/javascript">require(["mojo/signup-forms/Loader"], function (L) { L.start({ "baseUrl": "mc.us19.list-manage.com", "uuid": "7fe79480cbbff1efaeef1a833", "lid": "2c55d43b4a" }) })</script>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div id="rev_slider_211_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container testClass" data-alias="webproductlighthero" style="background-color: #f5f5f5; background-image: url(assets/img/backgrounds/Background.png); background-repeat: no-repeat; background-size: cover; background-position: center center; margin: 0px auto; padding: 0px; margin-top: 0px; margin-bottom: 0px;">
        <div id="ws-3d-parallax" class="rev_slider fullwidthabanner" style="display: none;">
            <ul>
                <!-- Slider  -->
                <li data-index="rs-699" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="1500" data-rotate="0" data-saveperformance="off">
                    <!-- Third Frame -->
                    <div class="tp-caption tp-resizeme rs-parallaxlevel-6" id="slide-699-layer-1" data-x="['center','center','center','center']" data-hoffset="['-1 54','-453','70','60']" data-y="['middle','middle','middle','bottom']" data-voffset="['30','50','211','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:right;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2500" data-responsive_offset="on" style="z-index: 5;">
                        <img src="assets/img/backgrounds/idea1.svg" alt="" id="imgparalex" style="width: 100%;" height="1200" data-ww="['1000px','100px','100px','300px']" data-hh="['500px','500px','100px','900px']" />
                    </div>


                    <!-- Button -->
                    <div class="tp-caption rs-parallaxlevel-8 center" id="slide-699-layer-8" data-x="['center','center','center','center']" data-hoffset="['30','30','200','80']" data-y="['middle','middle','middle','top']" data-voffset="['228','228','456','400']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;" data-style_hover="c:rgba(51, 51, 51, 1.00);bg:rgba(255, 255, 255, 1.00);" data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="1750" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"scrollbelow","offset":"px"}]' data-responsive_offset="on" data-responsive="off" style="z-index: 16; white-space: nowrap;">
                        <a class="btn ws-big-btn" style="margin: 0px;" href="Contact.aspx">Let's discuss</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>



    <Contactform:UserInfoBoxControl runat="server" ID="MyUserInfoBoxControl" />
    <section class="ws-works-section">
        <div class="container">
            <div class="row">
                <div class="ws-works-title">
                    <div class="col-sm-12 ">
                        <h3>Comprehensive Mobile App Solutions and IT services in Brisbane, Australia</h3>
                        <div class="ws-separator"></div>
                        <p class="text-center">
                            Big Brother Apps is a diverse IT solutions and services company that is a veteran of sorts having delivered comprehensive mobile app development
                            <br />
                            services for several domains.                            
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.05s, ease-in 20px'>
                    <div class="shadow">
                        <a href="The_Hurler.aspx">
                            <div class="ws-item-offer">
                                <figure>
                                    <img src="assets/img/works/Hurler.jpg" alt="The Hurler" class="img-responsive" />
                                </figure>
                            </div>
                            <div class="ws-works-caption text-center">
                                <%--<div class="ws-item-category">iOS / Android</div>--%>
                                <h3 class="ws-item-title">The Hurler</h3>
                                <div class="ws-item-separator"></div>
                                <%--<div class="ws-item-price">Case Study</div>--%>
                            </div>
                        </a>
                    </div>

                </div>
                <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.3s, ease-in 20px'>
                    <div class="shadow">
                        <a href="Clubman.aspx">
                            <figure>
                                <img src="assets/img/works/Clubman.jpg" alt="Clubman" class="img-responsive" />
                            </figure>
                            <div class="ws-works-caption text-center">
                                <%--<div class="ws-item-category">iOS / Android</div>--%>
                                <h3 class="ws-item-title">Clubman</h3>
                                <div class="ws-item-separator"></div>
                                <%--<div class="ws-item-price">Case Study</div>--%>
                            </div>
                        </a>
                    </div>

                </div>
                <%-- <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.5s, ease-in 20px'>
                    <a href="Extreme-Ireland.aspx">
                        <figure>
                            <img src="assets/img/works/ExtremeIreland.jpg" alt="ExtremeIreland" class="img-responsive" />
                        </figure>
                        <div class="ws-works-caption text-center">
                            <div class="ws-item-category">Android</div>
                            <h3 class="ws-item-title">Extreme Ireland</h3>
                            <div class="ws-item-separator"></div>
                            <div class="ws-item-price">Case Study</div>
                        </div>
                    </a>
                </div>--%>
                <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.5s, ease-in 20px'>
                    <div class="shadow">
                        <a href="CoolOff.aspx">
                            <figure>
                                <img src="assets/img/works/Cooloff.jpg" alt="Cooloff" class="img-responsive" />
                            </figure>
                            <div class="ws-works-caption text-center">
                                <%--<div class="ws-item-category">iOS / Android</div>--%>
                                <h3 class="ws-item-title">CoolOff</h3>
                                <div class="ws-item-separator"></div>
                                <%--<div class="ws-item-price">Case Study</div>--%>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.05s, ease-in 20px'>
                    <div class="shadow">
                        <a href="Deeds4Kids.aspx">
                            <figure>
                                <img src="assets/img/works/Rewardboard.jpg" alt="Deeds4Kids" class="img-responsive" />
                            </figure>
                            <div class="ws-works-caption text-center">
                                <%--<div class="ws-item-category">iOS / Android</div>--%>
                                <h3 class="ws-item-title">Deeds4Kids</h3>
                                <div class="ws-item-separator"></div>
                                <%--<div class="ws-item-price">Case Study</div>--%>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.3s, ease-in 20px'>
                    <div class="shadow">
                        <a href="Audilex.aspx">
                            <figure>
                                <img src="assets/img/works/Audilex.jpg" alt="Audilex" class="img-responsive" />
                            </figure>
                            <div class="ws-works-caption text-center">
                                <%--<div class="ws-item-category">iOS / Android</div>--%>
                                <h3 class="ws-item-title">Audilex</h3>
                                <div class="ws-item-separator"></div>
                                <%--<div class="ws-item-price">Case Study</div>--%>
                            </div>
                        </a>
                    </div>

                </div>
                <div class="col-sm-6 col-md-4 ws-works-item" data-sr='wait 0.5s, ease-in 20px'>
                    <div class="shadow">
                        <a href="Jiffy_Rides.aspx">
                            <div class="ws-item-offer">
                                <figure>
                                    <img src="assets/img/works/Jiffyrides.jpg" alt="JiffyRides" class="img-responsive" />
                                </figure>
                            </div>
                            <div class="ws-works-caption text-center">
                                <%--<div class="ws-item-category">iOS / Android</div>--%>
                                <h3 class="ws-item-title">Jiffy Rides</h3>
                                <div class="ws-item-separator"></div>
                                <%--<div class="ws-item-price">Case Study</div>--%>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ws-works-section">
        <div class="container">
            <div class="row">
                <div class="ws-subscribe-content text-center clearfix">
                    <div class="col-sm-12">
                        <h3>Our Offerings</h3>
                        <div class="ws-separator"></div>
                        <br />
                        <div class="col-sm-6 ws-journal-item">
                            <a href="#x">
                                <div class="ws-journal-image">
                                    <figure>
                                        <img src="~/../assets/img/offering/Mobile-app-development.png" alt="Alternative Text" style="width: 100%;" />
                                    </figure>
                                </div>
                                <div class="ws-journal-caption">
                                    <h3>Mobile App Development</h3>
                                    <p>Our mobile app developers power our comprehensive mobile application development services on hire and in-house engineers who are quite adept in meeting the requirements of our clients within a flexible engagement model.</p>
                                </div>
                            </a>
                        </div>

                        <div class="col-sm-6 ws-journal-item">
                            <a href="#x">
                                <div class="ws-journal-image">
                                    <figure>
                                        <img src="~/../assets/img/offering/iOS-app-development.png" alt="Alternative Text" style="width: 100%;" />
                                    </figure>
                                </div>
                                <div class="ws-journal-caption">
                                    <h3>iOS App Development</h3>
                                    <p>Our iPhone app development services stand the test of time owing to the expertise and experience of our team of iPhone app developers who continue to explore possibilities in leveraging the latest in the world of iOS, without giving up on user experience.</p>
                                </div>
                            </a>
                        </div>

                        <div class="col-sm-6 ws-journal-item">
                            <a href="#x">
                                <div class="ws-journal-image">
                                    <figure>
                                        <img src="~/../assets/img/offering/AndroidAppDevelopment.png" alt="Alternative Text" style="width: 100%;" />
                                    </figure>
                                </div>
                                <div class="ws-journal-caption">
                                    <h3>Android App Development</h3>
                                    <p>Our team of Android app developers consistently offers comprehensive Android app development for all the domains and industries while games play a significant part too. We ensure that each of our apps is robust, secure, and without loopholes in design or development while being part of the client’s vision and needs of its end users.</p>
                                </div>
                            </a>
                        </div>

                        <div class="col-sm-6 ws-journal-item">
                            <a href="#x">
                                <div class="ws-journal-image">
                                    <figure>
                                        <img src="assets/img/offering/Cross-platformapp-development.png" alt="Alternative Text" style="width: 100%;" />
                                    </figure>
                                </div>
                                <div class="ws-journal-caption">
                                    <h3>Cross-Platform App Development</h3>
                                    <p>Our expertise in cross-platform app development technologies works in our favor as our developers are keen to save on time for developing apps for multiple mobile operating systems, without compromising on efficiency and robustness insecurity. Our expertise in PhoneGap and Appcelerator Titanium along with others helps us meet client goals and objectives without any hassle.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <br />
    <section class="our-services ws-subscribe-content home2">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3>Our App Development Expertise – Salient Features</h3>
                    <div class="ws-separator"></div>
                    <p class="text-center">
                        Wide-Ranging App Solutions To Meet Evolving Demands Of the Modern Era.
                            <br />
                        <br />
                    </p>
                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/mobile-app-design.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Mobile App Design</h4>
                            Being a leading mobile app development company, Big Brother Apps employs embellished, competent, interactive, and desirable mobile app design that blends in with the brand image, the features of the app, and the vision of the makers to solve the requirements of the target audience.
                            
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/seamless-navigation.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Seamless Navigation</h4>
                            We design self-Intuitive apps that are known for their user-friendly navigation and a worthy user experience.
                           

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/userexperience.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>User Experience Beyond Compare</h4>
                            We proffer an amicable UX design that does not compromise on user experience, understanding what end users want and need.
                            
                            

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/skilled-team.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Robust Back-End</h4>
                            We ensure your data is secure with a strong and robust back-end structure and a filtered security mechanism for data governance and integrity.
                            

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/interactive-elements.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Interactive Elements</h4>
                            We design creative and visually appealing elements that adhere to design standards while encouraging maximum interactivity and seamlessness of operations.
                           

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/modern-design.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Modern UI</h4>
                            We design interactive UI design services for all platforms aligned with business requirements and evaluations that rest on the new UI concepts.
                           

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/expertiseinvariedtechnologies.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Expertise in varied technologies</h4>
                            We leverage our expertise with diverse SDKs, add-ons and other features that can be used for app design and development in diverse domains.
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br />
    <br />
    <br />
    <section class="our-services ws-subscribe-content home2">
        <div class="container">
            <div class="row">
                <br />
                <div class="col-sm-12 text-center">
                    <h3>Why opt for Big Brother Apps?</h3>
                    <div class="ws-separator"></div>                    
                    <p class="text-center">
                        Big Brother Apps is a dynamic firm that continues to deliver comprehensive app development and strategy solutions to its clients while being boundless in ambition. We have expanded our offerings globally while scaling our strength considerably, focusing on the latest tech solutions that matter to our client’s problems.                          

                    </p>
                    <br />                    
                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/android.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Android mobile app development services</h4>
                            With our team of app developers, we continue to offer adroit mobile app development services that ensure strong user experience with uncluttered design.
                            
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/focusonfunctionality.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Focus on functionality and design</h4>
                            We focus on clear functionality with low navigation paths so that our apps are conversant for less tech-savvy users. Our integration services facilitate data inflow and outflow without hassle but with proper authentication. 
                            <br />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/smartmobility.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Smart Mobility with security</h4>
                            <br />
                            With the influx of smart mobility, we have intensified our focus on security measures for M2M communications and strengthened data security while securing loopholes in codes and concepts. 
                            

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/seamlesscompletion.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Seamless Project Completion</h4>
                            We ensure that our projects are completed on time and budget, with responsive design standards and without compromise, even when saddled with time and budgetary constraints.  
                            
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/smartroi.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Smart ROI and Sound Strategy</h4>
                            Our smart mobility solutions and app development services rely on our expertise and experience thus ensuring exceptional ROI for our clients. We are never complacent with the development process that never compromises on quality while ensuring things are in place with a sound strategy.                            
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/frominitiationtocompletion.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>From Initiation to Implementation</h4>
                            We fulfill your app requirements to match your vision, quietly overcoming bumps and obstacles, adding dynamism to the design and interface too. Our development approach is quick and efficient thus ensuring swift app delivery.
                           
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="single-service HLarge">
                            <img class="lazy" src="~/../assets/img/home/CommitmentToExcellence.svg" alt="" style="height: 50px; text-align: center;" />
                            <h4>Commitment to Excellence</h4>
                            We continue to push the envelope in terms of innovative ideas and concepts. Our team of app developers along with testers, analysts, engineers, and project managers are consistent with their quality of work thus ensuring excellence, collectively and systematically.                            
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <br />
    <br />
    <br />
    <section id="ws-instagram-section ws-works-section home2">
        <div class="container">
            <div class="row vertical-align">
                <div class="col-sm-12 text-center">
                    <h3>Latest Blogs</h3>
                    <div class="ws-separator"></div>
                    <p>
                        We always try to remain abreast of latest trends in Technology as well as Industry.
                           
                            These blogs are humble attempt to share our knowledge, vision and thoughts with our readers and clients.
                            <br />
                    </p>
                    <br />
                    <br />
                    <asp:Repeater ID="rptBlogs" runat="server">
                        <ItemTemplate>
                            <div class="col-sm-3 ws-instagram-item" data-sr='wait 0.05s, ease-in 20px'>
                                <a href="<%# Page.GetRouteUrl("Blog",new { title= ToTitle(Eval("Title").ToString()), id=Eval("BlogId") }) %>">
                                    <img src="<%# Page.ResolveUrl("~/Files/BlogImages/"+Eval("ImageUrl")) %>" alt="<%# Eval("Title") %>" class="img-responsive" />
                                </a>
                                <div class="ws-works-caption text-center">
                                    <div class="ws-item-category"><a href="<%# Page.GetRouteUrl("Blog",new { title= Eval("Title"), id=Eval("BlogId") }) %>"><%# Eval("Title") %></a> </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <%-- <div class="col-sm-3">
                    <div class="ws-instagram-header">
                        
                        <br />
                        
                    </div>
                </div>--%>
            </div>

            <br />
            <%--<div class="ws-works-section">
                <div class="col-sm-7">
                    <div class="ws-checkout-billing">
                        <h3>Our Strategy</h3>
                        <p>
                            As a veteran solutions provider, we make sure that we accomplish your individual demands and fulfill your business needs. With advanced tools and technologies, our team can create customized mobile applications that align seamlessly with your present processes along with domain specific functions.
                            <br />
                            <br />
                            Unlike other top mobile app development companies in Ireland, Big Brother Apps does not confine their nature of work. Their team including analysts, developers, and designers, do absolute best to reap adroitly managed solutions, envisioned by their clients.
                            <br />
                            Hire app developers in Ireland with us, and we will make sure that you won’t be disappointed!
                            <br />
                            <br />
                            <b class="bluecolor">Brace yourself; a miracle is about to h’app’en!</b>
                        </p>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="ws-checkout-order">
                        <h2>Why US?</h2>
                        <ul class="list-group">
                            <li class="list-group-item">Experienced Team of Designers and Developers</li>
                            <li class="list-group-item">Profound knowledge of mobile technologies.</li>
                            <li class="list-group-item">Competent tried-and-tested development process</li>
                            <li class="list-group-item">Cost effective app development services</li>
                            <li class="list-group-item">Several robust mobile apps to their credit</li>
                            <li class="list-group-item">Systematic methodology and flexible engagements
                            </li>
                        </ul>
                    </div>
                </div>

            </div>--%>
        </div>
    </section>
    <br />
    <br />
    <br />
    <section class="ws-instagram-section ws-subscribe-content home2">
        <div class="container">
            <div class="row">
                <div class="ws-subscribe-content text-center clearfix">
                    <div class="col-sm-8 col-sm-offset-2">
                        <h3>Have an idea for app?</h3>
                        <div class="ws-separator"></div>
                        <div class="form-inline">
                            <a href="Contact.aspx" class="btn ws-btn-subscribe">Let's Talk</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br />
    <br />
    <br />
</asp:Content>
