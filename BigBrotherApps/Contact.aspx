﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="BigBrother.Contact1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Contact Big Brother Apps</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="Content/Plugins/Validate/jquery.unobtrusive-ajax.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=renderRecaptcha&render=explicit" async defer></script>
    <style>
         .ws-over {
            padding-top: 32px;
            height: 200px;
            width: 97%;
        }
        

        .caption {
            padding-top: 46px;
        }

        .thumbnail {
        }

        @media only screen and (max-width: 600px) {
            .ws-over {
                padding-top: 19px;
                margin-left: 22px;
                padding-left: 18px;
                height: 200px;
                width: 313px;
            }

            .caption {
                padding-top: 36px;
            }
        }

        /*@media only screen and (max-width: 768px) {
            .ws-overlay {
                margin-left: 22px;
                padding-left: 18px;
                height: 200px;
                width: 313px;
            }

            .caption {
                padding-top: 36px;
            }
        }*/



        @media only screen and (max-width: 600px) {
            .thumbnail {
                width: 313px;
                margin-left: 21px;
                height: 200px;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="assets/img/contactheader.jpg">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>Contact Us</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Parallax Header -->

    <!-- Page Content -->
    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-contact-page ws-item-title">
                <div class="col-sm-12">
                    <h3 style="text-align: center;">Free Quote</h3>
                    <div class="ws-separator"></div>
                    <p class="text-center">
                        Based on your requirements, we would be happy to share non-obligatory quote for your app’s concept.<br />
                        Please fill in the form on the right and we will connect with you very soon :)
                    </p>
                    <br />

                    <!-- General Information -->
                    <div class="col-sm-6">
                        <div class="ws-contact-info">
                            <br />
                            <h2><b>Free 30 minutes consultancy:</b></h2>
                            <br />
                            <p>If you have an idea and would like to discuss it with us, please contact us and we would be happy to arrange FREE consultation of 30 minutes whereby we can give you our feedback and reviews on your concept and help you out with few details based on our experience and latest market trends.</p>

                            <br />
                            <b>Big Brother Apps Pty Ltd – ABN 56 629 436 086</b><br />
                            <br />
                            149 wickham terrace,<br />
                            Spring Hill,<br />
                            QLD 4000.                        
                            <br />
                            <abbr title="Phone">P:</abbr>
                            <a href="tel:019081182" style="font-family: Merriweather,serif;">0423 220 534</a>
                            <br />
                            <abbr title="Phone">E:</abbr>
                            <a href="mailto:info@bigbrotherapps.com.au">info@bigbrotherapps.com.au</a>
                            <br />
                            <br />
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d625.816633342659!2d153.0250991749114!3d-27.46448073511358!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b915a02bd72d9e5%3A0x7599fb08126a870c!2sMorris+Towers%2C+149+Wickham+Terrace%2C+Spring+Hill+QLD+4000%2C+Australia!5e0!3m2!1sen!2sin!4v1546868205489" width="88%" height="200" frameborder="0" style="border: 0" allowfullscreen></iframe>
                            
                        </div>
                        <br />

                        <div class="col-sm-5 col-sm-4 ws-contact-office-item text-center" style="padding-left: 0px; width: 90%;" data-sr='wait 0.05s, ease-in 20px'>
                            <div class="thumbnail" style="height: 200px; padding: 0px;">
                                <img src="assets/img/Brisbane.jpg" alt="Alternative Text" style="height: 198px;" width="100%" />
                                <div class="ws-overlay ws-over">
                                    <div class="caption">
                               <strong style="color: white;">Australia</strong>

                                        <div class="ws-contact-separator"></div>
                                        <address style="color: white">
                                            149 wickham terrace, Spring Hill, QLD 4000. 
                                                <br />
                                            <br />
                                            <abbr title="Phone">P:</abbr>
                                            0423 220 534                                                 
                                            <br />                                            
                                            <abbr title="Email">E:</abbr>
                                            <a style="color: white;" href="mailto:info@bigbrotherapps.com.au">info@bigbrotherapps.com.au</a>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Contact Form -->
                    <div class="col-sm-6">
                        <div class="form-horizontal ws-contact-form">
                            <div class="form-group">
                                <asp:Label ID="spanMessage" Visible="false" Style="padding:0px;" CssClass="alert alert-success contactformmsg" runat="server"></asp:Label>
                            </div>
                            <div class="form-group">

                                <label class="control-label">Name <span>*</span></label>

                                <asp:TextBox ID="txtName" MaxLength="50" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="ContactForm1" ID="rqv" CssClass="red"
                                    ErrorMessage="This field is required" Display="Dynamic" runat="server" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                            </div>

                            <!-- Email -->
                            <div class="form-group">
                                <label class="control-label">Email <span>*</span></label>

                                <asp:TextBox ID="txtEmail" MaxLength="50" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="ContactForm1" Display="Dynamic" CssClass="red" ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtEmail" ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator Display="Dynamic" CssClass="red" ID="RegularExpressionValidator1" runat="server"
                                    ControlToValidate="txtEmail" ValidationGroup="ContactForm1" ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                                    ErrorMessage="Email address invalid"></asp:RegularExpressionValidator>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Phone <span>*</span></label>

                                <asp:TextBox ID="txtContactNumber" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="red" ID="RequiredFieldValidator1" ControlToValidate="txtContactNumber"
                                    runat="server" Display="Dynamic" ValidationGroup="ContactForm1" ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator Display="Dynamic" CssClass="red" ValidationGroup="ContactForm1" ValidationExpression="\d+"
                                    ControlToValidate="txtContactNumber" ID="RegularExpressionValidator3" runat="server"
                                    ErrorMessage="Phone Number Invalid"></asp:RegularExpressionValidator>
                            </div>
                            <div class="form-group">
                                <label class="control-label">City<span>*</span></label>
                                <asp:TextBox ID="txtAddress" MaxLength="500" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="red" ID="RequiredFieldValidator2" ControlToValidate="txtAddress"
                                    runat="server" Display="Dynamic" ValidationGroup="ContactForm1" ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Upload Docs</label>
                                <asp:FileUpload ID="fulAttachment" class="form-control" runat="server" />
                            </div>
                            <!-- Message -->
                            <div class="form-group">
                                <label class="control-label">Message <span>*</span></label>
                                <asp:TextBox TextMode="MultiLine" MaxLength="1000" class="form-control" ID="txtMessage"
                                    runat="server"></asp:TextBox>

                                <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="ContactForm1" ID="RequiredFieldValidator4"
                                    ControlToValidate="txtMessage" CssClass="red" runat="server" ErrorMessage="This field is required"></asp:RequiredFieldValidator>

                            </div>

                            <%--<div class="form-group">
                                <div class="checkbox">
                                    <label>

                                        <asp:CheckBox ID="chkConsultancy" Text="I want free 30 minutes <a target='blank' href='IT_Consultancy'>consultancy</a>" runat="server" />

                                    </label>
                                </div>
                            </div>--%>

                            <div class="form-group">
                                <br />
                                <div id="ReCaptchContainer"></div>
                                <label id="lblMessage" runat="server" clientidmode="static"></label>
                            </div>

                            <!-- Submit Button -->
                            <div class="form-group">
                                <asp:Button ID="btnSubmit" ValidationGroup="ContactForm1" CssClass="btn ws-big-btn"
                                    Text="Submit" runat="server" OnClick="btnSubmit_Click" />

                            </div>
                        </div>
                    </div>

                </div>
                <!-- Office Location -->

                <%--<div class="col-sm-12">
                    <div class="ws-contact-offices text-center">

                        <!-- Title -->
                        <h2>Office Location</h2>
                        <div class="ws-separator"></div>

                        <div class="row">
                            <!-- City -->
                            <div class="col-sm-5 col-sm-4 col-md-offset-4 ws-contact-office-item text-center" data-sr='wait 0.05s, ease-in 20px'>
                                <div class="thumbnail">
                                    <img src="assets/img/Brisbane.jpg" alt="Alternative Text" width="100%" />
                                    <div class="ws-overlay">
                                        <div class="caption">
                                            <strong>Australia
                                                
                                            </strong>

                                            <div class="ws-contact-separator"></div>
                                            <address>
                                                149 Wickham Terrace, Spring Hill, QLD 4000. 
                                                <br />
                                                <abbr title="Phone">P:</abbr>
                                                +6142 322 0534
                                                 <br />
                                                <br />
                                                <br />
                                                <abbr title="Email">E:</abbr>
                                                <a style="color: white;" href="mailto:info@bigbrotherapps.com.au">info@bigbrotherapps.com.au</a>
                                            </address>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                           

                        </div>

                    </div>
                </div>--%>
                <!-- End Office Location -->

            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <script type="text/javascript">  
        var istrue = false;
        $('#form1').submit(function () {
            var data = { 'captchaResponse': $('#g-recaptcha-response').val() };
            $.ajax({
                type: "POST",
                url: "Contact.aspx/IsReCaptchValid",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (data) {
                    var returnedstring = data.d;
                    if (returnedstring == false) {
                        jQuery('#lblMessage').css('color', 'red').html('Please check the Recaptcha');
                    }
                    else {
                        jQuery('#lblMessage').css('color', 'red').html(' ');
                        istrue = true;
                    }
                },
                failure: function (errMsg) {
                    alert(errMsg);
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });
            console.log(istrue);
            return istrue;
        });

        var your_site_key = '<%= ConfigurationManager.AppSettings["SiteKey"]%>';
        var renderRecaptcha = function () {
            grecaptcha.render('ReCaptchContainer', {
                'sitekey': your_site_key,
                'callback': reCaptchaCallback,
                theme: 'light', //light or dark    
                type: 'image',// image or audio    
                size: 'normal'//normal or compact    
            });
        };

        var reCaptchaCallback = function (response) {
            if (response !== '') {
                jQuery('#lblMessage').css('color', 'green').html(' ');
                istrue = true;
            }
        };

        jQuery('button[type="button"]').click(function (e) {
            var message = 'Please check the Recaptcha';
            if (typeof (grecaptcha) != 'undefined') {
                var response = grecaptcha.getResponse();
                (response.length === 0) ? (message = 'Captcha verification failed') : (message = ' ');
            }
            jQuery('#lblMessage').html(message);
            jQuery('#lblMessage').css('color', (message.toLowerCase() == 'success!') ? "green" : "red");
        });

    </script>
</asp:Content>


