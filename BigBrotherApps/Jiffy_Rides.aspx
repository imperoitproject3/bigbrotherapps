﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Jiffy_Rides.aspx.cs" Inherits="BigBrother.Jiffy_Rides" %>

<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Jiffy Rides App Case Study</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Breadcrumb -->
    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="Default.aspx">Home</a></li>
                <li><a href="Portfolio.aspx">Portfolio</a></li>
                <li class="active">Jiffy Rides</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Product Content -->
    <div class="container ws-page-container">
        <div class="row">

            <!-- Product Image Carousel -->
            <div class="col-sm-5">
                 <iframe src="https://marvelapp.com/4j35ai5?emb=1" width="100%" height="801" allowtransparency="true" frameborder="0"></iframe>
             
            </div>

            <!-- Product Information -->
            <div class="col-sm-7">
                <div class="ws-product-content">
                    <header>
                        <!-- Item Category -->

                        <!-- Title -->
                        <h3 class="ws-item-title">Jiffy Rides</h3>

                        <div class="ws-separator"></div>

                        <!-- Price -->


                        <!-- Quantity -->
                        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.jiffyrides.android&hl=en"><i class="fa fa-apple text-center fontsize24"></i></a>&nbsp;
                         <a target="_blank" href="https://itunes.apple.com/us/app/jiffy-rides/id970968243?mt=8"><i class="fa fa-android text-center fontsize24"></i></a>
                    </header>

                    <div class="ws-product-details">
                        <h3>Project Baseline</h3>
                        <p>
                            To create an application for people to commute to their work place and back home together with others who are having a similar route by splitting out the cost instead of commuting alone and spending more.<br />
                            The client is from USA, Kishore Kotapati who is famously known by his initials, KK
                        </p>
                        <br />
                        <br />
                        <h3 class="ws-item-title">The Problem</h3>
                        <p>
                            KK disliked the act of commuting from one place to another and spending hours in the traffic congestion every single day. Commuting between 30-50 miles in the USA is normal but people will either have to commute with their personal vehicle or use the public transport. 
Purchasing a personal vehicle is very costly plus the daily expense for fuel costs, maintenance etc.
                            <br />
                            <br />
                            The second option is the public transport, yes, it is a cheaper option  but there are some problems like: Fixed routes whereby a large number of commuters need to use a secondary commuting option such as walk for some miles, public transportation runs on a fixed a schedule whereby people need to make sure they are on time for the certain vehicle otherwise they might miss it.

                        </p>

                    </div>
                    <!-- Button -->
                    <a class="btn ws-big-btn" href="Contact.aspx">Let's Talk!</a>
                    <%--<a class="btn ws-big-btn" href="#ws-register-modal" data-toggle="modal">Let's Talk!</a><Contactform:UserInfoBoxControl runat="server" ID="MyUserInfoBoxControl" />--%>
                </div>
            </div>


            <div class="col-sm-12">
                <br />
                <br />
                <br />
                <h3 class="ws-item-title">The Solution</h3>
                <%--<b class="bluecolor"></b>--%>
                <p>
                    <br />
                    Carpooling Application to be called JIFFYRIDES!<br />
                    <br />
                    As a rider, all that needs to be done is, enter home and office address, find a local driver with extra seats, request to join for the ride and user will be picked up from the doorstep for a relaxing ride to your office/home.
                    <br />
                    <br />
                    As a driver, enter the departure time from home and office, people/neighbors will see the availability of drivers and request for a ride whereby drivers have the opportunity of accepting or rejecting and communicating with the rider through in app messaging feature, start the ride and get paid directly into your bank account.

                </p>
            </div>
        </div>
    </div>

    <div class="ws-products-description-content text-center">

        <!-- Item -->

        <!-- Item -->
        <div class="ws-product-description">
            <h3>App Features</h3>
            <ul class="item">
                <li class="list-group-item">Easy and quick registration for both drivers/riders through Facebook and LinkedIn.
                </li>
                <li class="list-group-item">The ability to search for your driver and passengers.
                </li>
                <li class="list-group-item">As a rider, the amenities of the car can be viewed.
                </li>
                <li class="list-group-item">Scheduling future commutes to avoid missing out rides.
                </li>
                <li class="list-group-item">As a rider, the ability to cancel rides without incurring any penalties.
                </li>
                <li class="list-group-item">View rating, reviews and recommend drivers and passengers to others.
                </li>
                <li class="list-group-item">Live tracking of the trip by viewing the drivers position.
                </li>
                <li class="list-group-item">Card payments method which guarantees cash is transferred securely.


                </li>

            </ul>
        </div>
    </div>
</asp:Content>

