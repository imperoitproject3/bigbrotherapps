﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Blog.aspx.cs" Inherits="BigBrother.Blog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Mobile App Development & App Marketing Blog by BigBrother Solutions</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<%= Page.ResolveUrl("~/") %>">Home</a></li>
                <li class="active">Blog</li>
            </ol>
        </div>
    </div>

    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-about-content col-sm-10 col-sm-offset-1">
                <!-- Information -->
                <p class="text-center">                                           
                            These blogs are designed to share our experience, knowledge, vision and thoughts with our valued readers and clients.

                </p>
            </div>
            <div class="ws-shop-page">
                <!-- Categories Content -->
                <div class="tab-content">
                    <!-- All -->
                    <div role="tabpanel" class="tab-pane fade in active" id="all">
                        <asp:Repeater ID="RV" runat="server">
                            <ItemTemplate>
                                <div class="col-sm-4 col-md-3 ws-works-item" data-sr='wait 0.05s, ease-in 20px'>
                                    <a href="<%# Page.GetRouteUrl("Blog",new { title= ToTitle(Eval("Title").ToString()), id=Eval("BlogId") }) %>">
                                        <div class="ws-item-offer">
                                            <!-- Image -->
                                            <figure>
                                                <img src="<%# Page.ResolveUrl("~/Files/BlogImages/"+Eval("ImageUrl")) %>" alt="<%# Eval("Title") %>" class="img-responsive" />
                                            </figure>
                                            <!-- Sale Caption -->
                                        </div>
                                        <div class="ws-works-caption text-center">
                                            <!-- Item Category -->
                                            <div class="ws-item-category"><%# Eval("Title") %></div>
                                            <!-- Title -->
                                           
                                           <%-- <div class="ws-item-separator"></div>
                                            <!-- Price -->
                                            <div class="ws-item-price">By <%# Eval("Author") %> in <p style="font-size:11px"> <%# Eval("Tag") %></p></div>--%>
                                        </div>
                                    </a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>