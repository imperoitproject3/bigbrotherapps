﻿create table BlogMaster(
BlogId int not null identity(1,1),
Title nvarchar(200) not null,
Author varchar(200) not null,
PublishDate datetime not null,
ImageUrl varchar(200) not null,
BlogBody nvarchar(max) not null,
Tag varchar(200) not null,
constraint PK_BlogId primary key clustered(BlogId))