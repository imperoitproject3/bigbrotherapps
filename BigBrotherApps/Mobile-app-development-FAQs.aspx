﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Mobile-app-development-FAQs.aspx.cs" Inherits="BigBrother.FAQs" %>

<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
            <title>Mobile App Development FAQs</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-faq-page">
                <div class="col-md-10 col-md-offset-1">

                    <!-- Tab Navabar -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#service" aria-controls="service" role="tab" data-toggle="tab">How to Start?</a></li>
                        <li role="presentation"><a href="#account" aria-controls="account" role="tab" data-toggle="tab">Development Process</a></li>
                        <li role="presentation"><a href="#buying" aria-controls="buying" role="tab" data-toggle="tab">Payment Process</a></li>
                        <li role="presentation"><a href="#shipping" aria-controls="shipping" role="tab" data-toggle="tab">After Sales</a></li>
                        <li role="presentation"><a href="#privacy" aria-controls="privacy" role="tab" data-toggle="tab">Production Rollout</a></li>
                    </ul>

                    <!-- Tab Panes -->
                    <div class="tab-content">

                        <!-- Services Panel -->
                        <div role="tabpanel" class="tab-pane fade in active ws-faq-pane-holder" id="service">

                            <div class="text-center">
                                <h3>I’ve an idea for an App, what’s the next step?</h3>
                                <div class="ws-separator"></div>
                                <p>It's awesome you have an idea, next step is researching your app and answering three important questions:</p>
                            </div>

                            <!-- Accordion Panel -->
                            <div class="ws-accordion">

                                <!-- Group -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle active" data-toggle="collapse" href="#collapseOne">1.	Can it be done? 
                                        </a>
                                    </div>

                                    <div id="collapseOne" class="accordion-body collapse in">
                                        <div class="accordion-inner">
                                            <p>Almost always yes!</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Group -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseTwo">2.	How much will it cost? 
                                        </a>
                                    </div>

                                    <div id="collapseTwo" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <p><a href="Contact.aspx">Request non-obligatory quote</a> along with 30 minutes of free consultancy, our prices are among the best available.</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Group -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseThree">3.	Can it be profitable? 
                                        </a>
                                    </div>

                                    <div id="collapseThree" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <p>
                                                With the right direction, most ideas have the potential to be profitable. 

                                                <br />
                                                At Square Root, we will help answer all of these questions almost immediately upon meeting any client. Our team will ensure to give unbiased, professional advice and feedback to all potential clients.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Group -->


                            </div>
                            <!-- End Accordion Panel -->
                        </div>
                        <!-- End Service Panel -->

                        <!-- Account Panel -->
                        <div role="tabpanel" class="tab-pane fade ws-faq-pane-holder" id="account">

                            <div class="text-center">
                                <h3>Development Process</h3>
                                <div class="ws-separator"></div>
                            </div>

                            <!-- Accordion Panel -->
                            <div role="tablist" aria-multiselectable="true" class="ws-accordion">
                                <div class="accordion">

                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" href="#collapse7">How do I convey my requirements?
                                            </a>
                                        </div>

                                        <div id="collapse7" class="accordion-body collapse">
                                            <div class="accordion-inner">
                                                <p>
                                                    If you are in Ireland, we are always ready to meet you one on one to discuss your requirements. This can surely help us to know each other’s in a better manner.
                                                    <br />
                                                    <br />
                                                    You can also submit your documents via email or if you don’t have it in written then you can share your ideas on Skype / Video or Telephonic call.
                                                    <br />
                                                    <br />
                                                    If you have wire-frames / prototypes ready then it’s awesome! If not, we can always prepare for you :)


                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseFive">How much does it cost to develop an App? 
                                            </a>
                                        </div>

                                        <div id="collapseFive" class="accordion-body collapse ">
                                            <div class="accordion-inner">
                                                <p>
                                                    A very difficult question to answer as there isn’t any answer to it. Every app is different because of its functionalities and capabilities and therefore every application will have its own costing. 
 <br />
                                                    <br />
                                                    At Square Root, we will give the potential clients an initial quote upon our first consultation. We will then give a detailed final quote after gathering more information from the client and discussing with our team. This quote will be based upon the final agreed project specifications.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Group -->
                                    <div class="accordion-group">

                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseSix">How long does it take to build an App?
                                            </a>
                                        </div>

                                        <div id="collapseSix" class="accordion-body collapse">
                                            <div class="accordion-inner">
                                                <p>
                                                    The length of time it takes to build an app can vary from one project to another ranging from few weeks to several months. We at Square Root can work within any realistic time-frame ensuring each project meets its deadline. We will not take on a project unless we are certain that we will meet the deadlines.

                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Group -->
                                    <div class="accordion-group">

                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseSeven">Will we sign a Non-Disclosure Agreement?
                                            </a>
                                        </div>

                                        <div id="collapseSeven" class="accordion-body collapse">
                                            <div class="accordion-inner">
                                                <p>Yes, in fact we provide each client with a signed NDA upon agreement of a projects specifications. We will also sign a NDA upon initial consultation if requested. </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Group -->

                                </div>

                            </div>
                            <!-- End Accordion Panel -->
                        </div>
                        <!-- Account Panel -->

                        <!-- Buying Panel -->
                        <div role="tabpanel" class="tab-pane fade ws-faq-pane-holder" id="buying">

                            <div class="text-center">
                                <h3>Payment Process</h3>
                                <div class="ws-separator"></div>
                            </div>

                            <!-- Accordion Panel -->
                            <div role="tablist" aria-multiselectable="true" class="ws-accordion">
                                <div class="accordion">

                                    <!-- Group -->
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseNine">What is the payment process?  
                                            </a>
                                        </div>

                                        <div id="collapseNine" class="accordion-body collapse in">
                                            <div class="accordion-inner">
                                                <p>
                                                    For each project, Square Root will suggest a payment process based upon the project’s milestones spread across the agreed upon time line. As each project is unique, Square Root are flexible to align to the customers payment needs whenever it’s possible.
                                                    <br />
                                                    Each milestone is linked with a payment, so once a particular milestone is achieved then we expect the client to pay for that. This way client doesn’t have to pay big amount altogether and can also see the progress before he makes the next payment.

                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Group -->
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseTen">Will Square Root help me obtain funding?
                                            </a>
                                        </div>

                                        <div id="collapseTen" class="accordion-body collapse">
                                            <div class="accordion-inner">
                                                <p>
                                                    Even in the current economy there still exists numerous sources of funding for mobile apps, from government agencies and private investors alike. We at Square Root will advise any of our potential clients about the investment sources that may help to ensure the deliverance of your project.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Group -->
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseEleven">If my app idea is potentially unprofitable, will Square Root give me honest feedback?
                                            </a>
                                        </div>

                                        <div id="collapseEleven" class="accordion-body collapse">
                                            <div class="accordion-inner">
                                                <p>
                                                    If your projects success is based upon future income, we will always give honest and unbiased professional feedback. We do not believe in taking projects that will not benefits our clients.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Group -->


                                </div>
                            </div>
                            <!-- End Accordion Panel -->
                        </div>
                        <!-- End Buying Panel -->

                        <!-- Shipping Panel -->
                        <div role="tabpanel" class="tab-pane fade ws-faq-pane-holder" id="shipping">

                            <div class="text-center">
                                <h3>After Sales</h3>
                                <div class="ws-separator"></div>
                            </div>

                            <!-- Accordion Panel -->
                            <div role="tablist" aria-multiselectable="true" class="ws-accordion">
                                <div class="accordion">

                                    <!-- Group -->
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseThirteen">Will you help me market my product after launch?
                                            </a>
                                        </div>

                                        <div id="collapseThirteen" class="accordion-body collapse in">
                                            <div class="accordion-inner">
                                                <p>
                                                    At Square Root, we do not see our relationship ending upon launch of an app. We offer our clients continued support and maintenance for their app along with professional marketing advice to help in promoting their project.


                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Group -->
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseFourteen">Will Square Root Host, update and maintain my app after its launch?
                                            </a>
                                        </div>

                                        <div id="collapseFourteen" class="accordion-body collapse">
                                            <div class="accordion-inner">
                                                <p>
                                                    Yes, the success of your project is very important to us. We offer a very comprehensive after-sales service to all our customers including a complimentary 3 months support after the app launches. We have plans with nominal rates.

                                                </p>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <!-- End Accordion Panel -->
                        </div>
                        <!-- Shipping Panel -->

                        <!-- Privacy Policy Panel Panel -->
                        <div role="tabpanel" class="tab-pane fade ws-faq-pane-holder" id="privacy">

                            <div class="text-center">
                                <h3>Production Rollout</h3>
                                <div class="ws-separator"></div>
                            </div>

                            <!-- Accordion Panel -->
                            <div class="ws-accordion">

                                <!-- Group -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle active" data-toggle="collapse" href="#collapseSeventeen">Who owns the app?
                                        </a>
                                    </div>

                                    <div id="collapseSeventeen" class="accordion-body collapse in">
                                        <div class="accordion-inner">
                                            <p>The client will retain the ownership of the application and its source codes upon completion of the project and payments.</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Group -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseEighteen">Will you provide  us with app source code?
                                        </a>
                                    </div>

                                    <div id="collapseEighteen" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <p>Yes, once the final payment is made we shall provide you with entire source code, keys, credentials and everything related to your project.</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Group -->
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseNineteen">How much does it take for the application to go live?
                                        </a>
                                    </div>

                                    <div id="collapseNineteen" class="accordion-body collapse">
                                        <div class="accordion-inner">
                                            <p>After the development is completed and approved by the client, we upload the app to the Play store for the Android app’s and Apple Store for the iOS app’s. While Android app gets live within few hours, iOS app can take up to 7 days to get approval by the App Store Reviewing Team.</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Group -->


                            </div>
                            <!-- End Accordion Panel -->
                        </div>
                        <!-- End Privacy Policy Panel -->

                    </div>
                    <!-- End Tab Panes -->

                </div>
            </div>
        </div>
    </div>
</asp:Content>

