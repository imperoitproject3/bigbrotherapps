﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Extreme-Ireland.aspx.cs" Inherits="BigBrother.ExtremeIreland" %>

<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Extreme Ireland App Case Study</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Breadcrumb -->
    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="Default.aspx">Home</a></li>
                <li><a href="Portfolio.aspx">Portfolio</a></li>
                <li class="active">Extreme Ireland</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Product Content -->
    <div class="container ws-page-container">
        <div class="row">

            <!-- Product Image Carousel -->
            <div class="col-sm-5">
               
                <iframe src="https://marvelapp.com/84f50f4?emb=1" width="100%" height="801" allowtransparency="true" frameborder="0"></iframe>
            </div>

            <!-- Product Information -->
            <div class="col-sm-7">
                <div class="ws-product-content">
                    <header>
                        <!-- Item Category -->

                        <!-- Title -->
                        <h3 class="ws-item-title">Extreme Ireland</h3>

                        <div class="ws-separator"></div>

                        <!-- Price -->


                        <!-- Quantity -->
                        <%--<a target="_blank" href="https://itunes.apple.com/us/app/cool-off-app/id1119480248?ls=1&mt=8"><i class="fa fa-apple text-center fontsize24"></i></a>&nbsp;--%>
                        <a target="_blank" href="https://play.google.com/store/apps/details?id=ie.irishdaytours.android.english"><i class="fa fa-android text-center fontsize24"></i></a>
                    </header>

                    <div class="ws-product-details">
                        <h3>Project Baseline</h3>
                        <p>
                            It is never easy for domestic and specifically international tourist to explore unknown place without any hesitation and challenges.
                            <br />
                            <br />
                            Tourist always seek a local guide in order to get convenience and to learn about the place which may cost extra. A digital, user friendly solution for tourist with all necessary feature may make tourist independent and help a lot to understand about the Ireland.
                        </p>
                        <br />
                        <h3>The Problems and Solutions</h3>
                        <p>
                            1.It is difficult for the tourist to get accurate information about unknown place from one place. Visitors need to rely on local people, books or guide for all desired information.
                            <br />
                            However, using this application, tourist can access all the information at on their fingertips and can learn something new about the place they visit.
                            <br />
                            <br />
                            2.Nowadays, digital maps and navigation are easily available but challenge with tourist is to identify right destination.
                            <br />
                            However, this app suggests multiple destinations map along with navigation.
                            <br />
                            <br />
                            3.Moreover, linguistic barrier is a common communication problem for the tourist when the place is unfamiliar.
However, this application provides five different languages such as Italian, Spanish, English, German and French are available along with the audio functionality. 
                            <br />
                            <br />
                            4.It also becomes expensive to access the information at roaming charges and besides tourist will spend all the time looking into cell phone instead of enjoying.
                            <br />
                            However, using this application, visitors can educate themselves of allied place while exploring by listening to the audio. They won’t have to spend much longer time in reading the information. Just put on the headphone in ear, play the audio and start exploring by your convenient language.
                            <br />
                        </p>
                    </div>
                    <!-- Button -->
                    <a class="btn ws-big-btn" href="Contact.aspx">Let's Talk!</a>
                    <%--<a class="btn ws-big-btn" href="#ws-register-modal" data-toggle="modal">Let's Talk!</a><Contactform:UserInfoBoxControl runat="server" ID="MyUserInfoBoxControl" />--%>
                </div>
            </div>
            <br />

        </div>
    </div>
    <div class="ws-products-description-content text-center">

        <!-- Item -->

        <!-- Item -->
        <div class="ws-product-description">
            <h3>App Features:</h3>
            <ul class="item">
                <li class="list-group-item">Digital information provider of cities of Ireland.

                </li>
                <li class="list-group-item">This app avails maps and navigation to make easier for you to spot the destination.

                </li>
                <li class="list-group-item">Multilingual application. Consists five languages such as Italian, Spanish, English, German and French together with the audio function

                </li>
                <li class="list-group-item">It works on both online and offline mode.



                </li>

            </ul>
        </div>
    </div>
</asp:Content>

