﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigBrother
{
    public partial class Contact : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ImperoIT.Inquirys ObjInquiry = new ImperoIT.Inquirys();
            string Name = txtName.Text.Trim();
            string Email = txtEmail.Text.Trim();
            string Address = txtAddress.Text.Trim();
            string Number = txtContactNumber.Text.Trim();
            string Message = txtMessage.Text.Trim();
            string FilePath = "";
            if (fulAttachment.HasFile)
            {
                string FileName = fulAttachment.FileName;
                string Path = Server.MapPath("Attachment");
                fulAttachment.SaveAs(Path + "\\" + FileName);
                ObjInquiry.Attachment = FileName;
                FilePath = Path + "\\" + FileName;
            }
            ObjInquiry.Name = Name;
            ObjInquiry.EmailAddress = Email;
            ObjInquiry.Address = Address;
            ObjInquiry.Message = Message;
            ObjInquiry.Number = Number;
            ImperoIT.Email ObjEMail = new ImperoIT.Email();

            StringBuilder ContentForAdmin = new StringBuilder();
            ContentForAdmin.Append("Person With Name: " + Name + " has contacted you on your website www.BigBrother.ie");
            ContentForAdmin.Append("\n\n\r");

            ContentForAdmin.Append("\n\n\r");
            ContentForAdmin.Append("IT Consultancy Required " + chkConsultancy.Checked);
            ContentForAdmin.Append("His Contact Number is: " + Number);
            ContentForAdmin.Append("\n\n\r");
            ContentForAdmin.Append("His Email Address is: " + Email);
            ContentForAdmin.Append("\n\n\r");
            ContentForAdmin.Append("His Address is: " + Address);
            ContentForAdmin.Append("\n\n\r");
            ContentForAdmin.Append("His Message is : " + Message);
            ContentForAdmin.Append("\n\n\r");
            ContentForAdmin.Append("Thank You");


            ObjEMail.SendEmail(ContentForAdmin, "notification@imperoit.com", "raza@imperoit.com", "mena.theodorou@gmail.com", "", "New Inquiry on www.bigbrotherapps.com.au", FilePath);

            int success = ObjInquiry.AddInquiry();

            if (success == 1)
            {
                txtMessage.Text = txtAddress.Text = txtContactNumber.Text = txtName.Text = txtEmail.Text = "";
                chkConsultancy.Checked = false;
                spanMessage.Visible = true;
                spanMessage.Text = "Thanks for your Inquiry. We shall be in touch with you within 24 working hours.";
            }

        }

    }
}