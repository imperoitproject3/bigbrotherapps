﻿$(function () {
    $('.textarea-maxlength').maxlength({
        alwaysShow: true,
        warningClass: "tag tag-success",
        limitReachedClass: "tag tag-danger",
    });

    $('#Regions').select2({
        placeholder: "Select Regions",
        tags: true
    });
    //$('#Countries').select2({
    //    placeholder: "Select Countries",
    //    tags: true
    //});

    $('#Categories').select2({
        placeholder: "Select Categories",
        tags: true
    });

    $('#StartDateUTC').datetimepicker({
        format: 'DD/MM/YYYY HH:mm'
    });

    $('#EndDateUTC').datetimepicker({
        format: 'DD/MM/YYYY HH:mm'
    });

    $.validator.methods.date = function (value, element) {
        return this.optional(element) || moment(value, "DD/MM/YYYY HH:mm", true).isValid();
    }

    if ($('#isWorldWide').is(':checked')) {
        $('#Regions').val(null).trigger('change');
        $('#Regions').select2({
            placeholder: "Select Regions",
            disabled: true
        });

        $('#isExclusive').attr('disabled', 'disabled');

        //$('#Countries').val(null).trigger('change');
        //$('#Countries').select2({
        //    placeholder: "Select Countries",
        //    disabled: true
        //});        
    }
});

$('#Regions').on('select2:select', function (e) {
    $('#SelectedRegions').val($(this).select2("val").join(','));
    $('#frmUpdateCountries').submit();
});
$('#Regions').on('select2:unselect', function (e) {
    if ($(this).select2("val"))
        $('#SelectedRegions').val($(this).select2("val").join(','));
    else
        $('#SelectedRegions').val(null);
    $('#frmUpdateCountries').submit();
});

$('#isWorldWide').on('change', function () {
    if ($(this).is(':checked')) {
        $('#Regions').val(null).trigger('change');
        $('#Regions').select2({
            placeholder: "Select Regions",
            disabled: true
        });
        $('#isExclusive').attr('disabled', 'disabled');
        //$('#Countries').val(null).trigger('change');
        //$('#Countries').select2({
        //    placeholder: "Select Countries",
        //    disabled: true
        //});
        $('#bootstrap-duallistbox-nonselected-list_Countries').find('option').each(function () {
            $(this).remove();
        });
        $('#bootstrap-duallistbox-selected-list_Countries').find('option').each(function () {
            $(this).remove();
        });
        $(".bootstrap-duallistbox-container").find("*").prop("disabled", true);
    }
    else {
        $('#Regions').select2({
            placeholder: "Select Regions",
            tags: true,
            disabled: false
        });
        $('#isExclusive').removeAttr('disabled', 'disabled');
        //$('#Countries').select2({
        //    placeholder: "Select Countries",
        //    tags: true,
        //    disabled: false
        //});
        $(".bootstrap-duallistbox-container").find("*").prop("disabled", false);
    }
});

function fnReloadCountrySelect2() {
    //$('#Countries').select2({
    //    placeholder: "Select Countries",
    //    tags: true
    //});
    $('.duallistbox').bootstrapDualListbox();
}

$('#StartDateUTC').on('dp.change', function () {
    var isValid = false;
    //var CurrentDate = moment(moment(new Date()).format('DD/MM/YYYY HH:mm'), 'DD/MM/YYYY HH:mm');
    var StartDateUTC = moment($(this).val(), 'DD/MM/YYYY HH:mm');
    var EndDateUTC = moment($('#EndDateUTC').val(), 'DD/MM/YYYY HH:mm');

    //if (StartDateUTC.isValid()) {
    //    isValid = moment(CurrentDate).isSameOrBefore(moment(StartDateUTC));
    //    if (isValid == false) {
    //        toastr.error('Start date must be greater than or equal to current date', 'Error !');
    //        $(this).val('');
    //        return false;
    //    }
    //}
    if (StartDateUTC.isValid() && EndDateUTC.isValid()) {
        var isValid = moment(StartDateUTC).isSameOrBefore(moment(EndDateUTC));
        if (isValid == false) {
            toastr.error('Start date is less then end date', 'Error !');
            $(this).val('');
        }
    }
});

$('#EndDateUTC').on('dp.change', function () {
    var isValid = false;
    //var CurrentDate = moment(new Date(), 'DD/MM/YYYY HH:mm');
    var StartDateUTC = moment($('#StartDateUTC').val(), 'DD/MM/YYYY HH:mm');
    var EndDateUTC = moment($(this).val(), 'DD/MM/YYYY HH:mm');

    //if (EndDateUTC.isValid()) {
    //    isValid = moment(CurrentDate).isSameOrBefore(moment(EndDateUTC));
    //    if (isValid == false) {
    //        toastr.error('End date must be greater than or equal to current date', 'Error !');
    //        $(this).val('');
    //        return false;
    //    }
    //}
    if (StartDateUTC.isValid() && EndDateUTC.isValid()) {
        var isValid = moment(StartDateUTC).isSameOrBefore(moment(EndDateUTC));
        if (isValid == false) {
            toastr.error('End date is greater then start date', 'Error !');
            $(this).val('');
        }
    }
});

$('.btnDeleteImage').on('click', function () {
    $(this).parent().parent().parent().find('.uploaded-image').val(null);
    $(this).parent().slideUp('remove');
});