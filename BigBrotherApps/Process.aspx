﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Process.aspx.cs" Inherits="BigBrother.Process" %>

<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>App Development Process</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="#">About</a></li>

                <li class="active">How it works</li>
            </ol>
        </div>
    </div>
    <div class="container ws-works-section">
        <div class="row">
            <div class="ws-about-content col-sm-10 col-sm-offset-1">
                <!-- Information -->
                <p class="text-center">
                    Our mobile apps fulfill client needs and simplify business operations with app-based solutions. Blessed with a sense of creativity, Big Brother Apps ensures that all apps are superior apps, with proper and navigation guaranteeing a memorable feedback that features our signature class. 
                    <br />
                    <br />
                    Our systematic process for mobile app development process includes:                              
                </p>
            </div>
        </div>
    </div>

     <section class="ws-works-section" style="padding-top:0px;">
                <div class="container">
                    <div class="row">
                        <div class="ws-subscribe-content text-center clearfix">
                            <div class="col-sm-12"> 
                                <div class="col-sm-4 ws-journal-item">
                                    <a href="#x">
                                        <div class="ws-journal-image">
                                            <figure>
                                                <img src="~/../assets/img/process/Comprehensive_requirement.png" alt="Alternative Text" style="width: 100%; height: 200px;" />
                                            </figure>
                                        </div>
                                        <div class="ws-journal-caption">
                                            <h3>Requirement Analysis</h3>

                                            <p class="text-center">Detailed review of your requirements with assessment and analysis of the idea-to-production process with honesty and experience.</p>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-sm-4 ws-journal-item">
                                    <a href="#x">
                                        <div class="ws-journal-image">
                                            <figure>
                                                <img src="~/../assets/img/process/Impeccable_Design.png" alt="Alternative Text" style="width: 100%; height: 200px;" />
                                            </figure>
                                        </div>
                                        <div class="ws-journal-caption">
                                            <h3>Impeccable Design</h3>
                                            <p class="text-center">Our experienced team of web and app designers working to create unique designs aligned with brand identity to ensuring better credibility and content.</p>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-sm-4 ws-journal-item">
                                    <a href="#x">
                                        <div class="ws-journal-image">
                                            <figure>
                                                <img src="~/../assets/img/process/Seamless_Development_Approach.png" alt="Alternative Text" style="width: 100%; height: 200px;" />
                                            </figure>
                                        </div>
                                        <div class="ws-journal-caption">
                                            <h3>Seamless Development</h3>
                                            <p class="text-center">Our team of app developers adhere to a systematic development approach following the agile process for all projects to our clients.</p>
                                        </div>
                                    </a>
                                </div>

                            </div>

                            <div class="col-sm-12">
                                <div class="col-sm-4 ws-journal-item">
                                    <a href="#x">
                                        <div class="ws-journal-image">
                                            <figure>
                                                <img src="assets/img/process/Stringent_Testing_Approach.png" alt="Alternative Text" style="width: 100%; height: 200px;" />
                                            </figure>
                                        </div>
                                        <div class="ws-journal-caption">
                                            <h3>Stringent Testing Approach</h3>
                                            <p class="text-center">We ensure that apps undergo a robust and stringent test process to ensure the fastest load times and no bugs.</p>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-sm-4 ws-journal-item">
                                    <a href="#x">
                                        <div class="ws-journal-image">
                                            <figure>
                                                <img src="~/../assets/img/process/Creative_App_Customization.png" alt="Alternative Text" style="width: 100%; height: 200px;" />
                                            </figure>
                                        </div>
                                        <div class="ws-journal-caption">
                                            <h3>Creative App Customizations</h3>
                                            <p class="text-center">We personalise apps constantly with help from experts who are familiar with modern and trending add-ons and business connectors.</p>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-sm-4 ws-journal-item">
                                    <a href="#x">
                                        <div class="ws-journal-image">
                                            <figure>
                                                <img src="~/../assets/img/process/Timely-Support-Maintanance.png" alt="Alternative Text" style="width: 100%; height: 200px;" />
                                            </figure>
                                        </div>
                                        <div class="ws-journal-caption">
                                            <h3>Timely Support</h3>
                                            <p class="text-center">Support and maintenance for all apps deployed in the app store for all mobile operating systems  at anytime and maintained under agreement. Your success is our success!</p>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            
                        </div>
                    </div>
                </div>
            </section>
    <div class="ws-products-description-content text-center">
        
       
        

        

       

       

        

        
    </div>

</asp:Content>


