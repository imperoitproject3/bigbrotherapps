﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImperoIT
{
    public sealed class Utility
    {
        public static string ToTitle(string value)
        {
            return value.Replace(" ", "").ToLower();
        }
    }
}