﻿namespace BigBrother.Models
{
    public class BlogMasterModel
    {
        public int BlogId { get; set; }
        public string Title { get; set; }
        public string Tag { get; set; }
        public string Author { get; set; }
        public System.DateTime PublishDate { get; set; }
        public string ImageUrl { get; set; }
        public string BlogBody { get; set; }
    }
}