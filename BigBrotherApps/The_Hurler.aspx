﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="The_Hurler.cs" Inherits="BigBrother.The_Hurler" %>

<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>The Hurler App Case Study</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Breadcrumb -->
    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="Default.aspx">Home</a></li>
                <li><a href="Portfolio.aspx">Portfolio</a></li>
                <li class="active">The Hurler</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Product Content -->
    <div class="container ws-page-container">
        <div class="row">

            <!-- Product Image Carousel -->


            <div class="col-sm-5">
                <%--  <div id="ws-products-carousel" class="owl-carousel">
                    <div class="item">
                        <img src="assets/img/works/The Hurler/1.png" class="img-responsive" />
                    </div>
                    <div class="item">
                        <img src="assets/img/works/The Hurler/2.png" class="img-responsive" />
                    </div>
                    <div class="item">
                        <img src="assets/img/works/The Hurler/3.png" class="img-responsive" />
                    </div>
                </div>--%>
                <iframe src="https://marvelapp.com/438dhbj?emb=1" width="100%" height="901" allowtransparency="true" frameborder="0"></iframe>
            </div>
            <div class="col-sm-7">
                <div class="ws-product-content">
                    <header>
                        <!-- Item Category -->

                        <!-- Title -->
                        <h3 class="ws-item-title">The Hurler</h3>

                        <div class="ws-separator"></div>

                        <!-- Price -->


                        <!-- Quantity -->
                        <a target="_blank" href="https://itunes.apple.com/gb/app/gaa-player-development-the-hurler/id1241636178?mt=8"><i class="fa fa-apple text-center fontsize24"></i></a>&nbsp;
                         <a target="_blank" href="https://play.google.com/store/apps/details?id=com.thehurler&hl=en"><i class="fa fa-android text-center fontsize24"></i></a>
                    </header>

                    <div class="ws-product-details">
                        <h3>Project Baseline</h3>
                        <p>To create an application for the GAA coaches to access the abilities and progress/development of the players through a professional based skills model.</p>
                        <br />
                        <br />
                        <h3>The Problems and Solutions</h3>
                        <p>
                            Before launching of this application, users would sit down and record everything on the excel sheet daily to keep records of their players progress and development.
                            <br />
                            <br />
                            Using the excel sheet is usually not suitable for maintaining large data. Some of the reasons to this are:<br />
                            <br />
                            -Excel sheets are vulnerable to fraud.
                            <br />
                            -Excel sheets are prone to human errors<br />
                            -The users precious time are wasted.<br />
                            <br />
                            <i>The hurler application therefore reduces the workload for the people as the application has been designed in the simplest and quickest way for the users to record the progress and development.</i>

                        </p>

                    </div>
                    <!-- Button -->
                    <a class="btn ws-big-btn" href="Contact.aspx">Let's Talk!</a>
                    <%--<a class="btn ws-big-btn" href="#ws-register-modal" data-toggle="modal">Let's Talk!</a><Contactform:UserInfoBoxControl runat="server" ID="MyUserInfoBoxControl" />--%>
                </div>
            </div>


        </div>
    </div>
    <div class="ws-products-description-content text-center">

        <!-- Item -->

        <!-- Item -->
        <div class="ws-product-description">
            <h3>App Features</h3>
            <ul class="item">
                <li class="list-group-item">This application can be used by both, the GAA coaches and guest’s hurler.  </li>
                <li class="list-group-item">The application displays a list of clubs for the coaches to select and asses their players.</li>
                <li class="list-group-item">There are two types of assessments, skills as the first and fitness as the second. </li>
                <li class="list-group-item">The players can be added according to the types of assessments and according to age categories as well.</li>
                <li class="list-group-item">The users will be shown a live timer with options to start and stop, continue with the remaining attempts and end the particular challenge. Users can then also decide whether they want to access the same challenge for another player or they want to switch the challenge for the current player.

                </li>

            </ul>
        </div>
    </div>

</asp:Content>

