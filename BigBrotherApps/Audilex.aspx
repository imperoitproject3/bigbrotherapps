﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Audilex.aspx.cs" Inherits="BigBrother.Audilex" %>
<%@ Register TagPrefix="Contactform" TagName="UserInfoBoxControl" Src="~/Contact.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
            <title>Audliex App Case Study</title>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Breadcrumb -->
    <div class="ws-breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="Default.aspx">Home</a></li>
                <li><a href="Portfolio.aspx">Portfolio</a></li>
                <li class="active">Audilex</li>
            </ol>
        </div>
    </div>
    <!-- End Breadcrumb -->

    <!-- Product Content -->
    <div class="container ws-page-container">
        <div class="row">

            <!-- Product Image Carousel -->
            <div class="col-sm-5">
               <%-- <div id="ws-products-carousel" class="owl-carousel">
                    <div class="item">
                        <img src="assets/img/works/Audilex/1.png" class="img-responsive" />
                    </div>
                    <div class="item">
                        <img src="assets/img/works/Audilex/2.png" class="img-responsive" />
                    </div>
                    <div class="item">
                        <img src="assets/img/works/Audilex/3.png" class="img-responsive" />
                    </div>
                </div>--%>
                <iframe src="https://marvelapp.com/6j13e7h?emb=1" width="100%" height="801" allowTransparency="true" frameborder="0"></iframe>
            </div>

            <!-- Product Information -->
            <div class="col-sm-7">
                <div class="ws-product-content">
                    <header>
                        <!-- Item Category -->

                        <!-- Title -->
                        <h3 class="ws-item-title">Audilex</h3>

                        <div class="ws-separator"></div>

                        <!-- Price -->


                        <!-- Quantity -->
                        <a target="_blank" href="https://itunes.apple.com/us/podcast/audilex/id1209753970"><i class="fa fa-apple text-center fontsize24"></i></a>&nbsp;
                         <a target="_blank" href="https://play.google.com/store/apps/details?id=com.audilex&hl=en_GB"><i class="fa fa-android text-center fontsize24"></i></a>
                    </header>

                    <div class="ws-product-details">
                        <h3>Project Baseline</h3>
                        <p>
                            To create an app for uploading precedents of major legal cases of the U.K for educational purposes.<br />
                            Timur Boltaev who develops comprehensive database of U.K case decisions in audio format. 
                        </p>
                        <br />
                        <br />
                        <h3>The Problems and Solutions</h3>
                        <p>
                            In the past centuries, there have been a large number of landmark cases which haven’t been translated and not easily available as well. The Audilex application is now live with a great list of precedents for students who are pursuing their education in the legal field. 
                            <br />
                            <br />
                            The following are some of the categories of precedents case:                           
                            <br />

                            Company law, M & A, Constitutional law, Contracts, International law, Criminal law, Torts and delicti, etc.
                            <br />
                            <br />
                            <i>Those who are pursuing law studies or interested in gaining some knowledge from the law field can make a great use of this application. </i>

                        </p>

                    </div>
                    <!-- Button -->
                    <a class="btn ws-big-btn" href="Contact.aspx">Let's Talk!</a>
                    <%--<a class="btn ws-big-btn" href="#ws-register-modal" data-toggle="modal">Let's Talk!</a><Contactform:UserInfoBoxControl runat="server" ID="MyUserInfoBoxControl" />--%>
                </div>
            </div>
        </div>
    </div>
    <div class="ws-products-description-content text-center">

        <!-- Item -->

        <!-- Item -->
        <div class="ws-product-description">
            <h3>App Features</h3>
            <ul class="item">
                <li class="list-group-item">Facebook login and the application will fetch your necessary details.
                </li>
                <li class="list-group-item">The list of precedents has been formatted in English language as it is the most commonly used language.
                </li>
                <li class="list-group-item">Precedents are uploaded in audio format so that users can practice hearing skills and get a competitive advantage because in the law field it is very important to be able to hear accurately. Precedents are usually in reading format but we have formatted it in audio so that users can also hear them while they are performing recreational activities.
                </li>
                <li class="list-group-item">The audios are available for downloading so that users can hear them in offline mode.
                </li>
                <li class="list-group-item">Points and time system for a day, week and month so that all those dedicated students can track on how much they have covered. In addition, this system is also available for each category so that users know how much time they have spent in each category.
                </li>
                <li class="list-group-item">Quiz feature implemented for users to answer few question based upon the recent precedents they heard/studied. 


                </li>

            </ul>
        </div>
    </div>

</asp:Content>

