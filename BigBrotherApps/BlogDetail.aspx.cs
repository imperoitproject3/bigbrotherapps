﻿using BigBrother.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace BigBrother
{
    public partial class BlogDetail : System.Web.UI.Page
    {
        public static string ToTitle(string value)
        {
            return value.Replace(" ", "-").ToLower();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.RequestContext.RouteData.Values.ContainsKey("title") && Request.RequestContext.RouteData.Values.ContainsKey("id"))
                {
                    string title = Request.RequestContext.RouteData.Values["title"].ToString();
                    int BlogId = Convert.ToInt32(Request.RequestContext.RouteData.Values["id"]);

                    BindBlogDetail(BlogId);
                    BindRecentBlogs(BlogId);
                }
                else
                    Response.Redirect("~/blog.aspx");
            }
            catch
            {
                Response.Redirect("~/blog.aspx");
            }
        }

        private void BindBlogDetail(int BlogId)
        {
            BigBrotherEntities _ctx = new BigBrotherEntities();

            BlogMaster objBlog = _ctx.BlogMaster.FirstOrDefault(x => x.BlogId == BlogId);

            if (objBlog == null)
                Response.Redirect("~/blog.aspx");

            ltTitle.Text = ltBreadCrumb.Text = ltlBlogTitle.Text = objBlog.Title;
            ltPublished.Text = "Published on " + objBlog.PublishDate.ToLongDateString() + " by " + objBlog.Author;
            divBlogBody.InnerHtml = objBlog.BlogBody;
        }

        private void BindRecentBlogs(int BlogId)
        {
            BigBrotherEntities _ctx = new BigBrotherEntities();

            List<BlogMaster> objBlog = _ctx.BlogMaster.Where(x => x.BlogId != BlogId).OrderByDescending(x => x.PublishDate).Take(10).ToList();

            if (objBlog != null && objBlog.Any())
            {
                RPRecentPost.DataSource = objBlog;
            }
            else
            {
                RPRecentPost.DataSource = null;
                ltlNoPost.Text = "No recent post.";
            }
            RPRecentPost.DataBind();
        }
    }
}