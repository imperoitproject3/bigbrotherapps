﻿using System.Web.Optimization;

namespace BigBrother.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/Styles").Include(
                    "~/assets/css/bootstrap.min.css",
                    "~/assets/css/main.css",
                    "~/assets/css/animate.min.css",
                    "~/assets/js/plugins/revolution/css/settings.css",
                    "~/assets/js/plugins/revolution/css/layers.css",
                    "~/assets/js/plugins/revolution/css/navigation.css",
                    "~/assets/js/plugins/owl-carousel/owl.carousel.css"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                    "~/assets/js/plugins/jquery-1.11.3.min.js",
                    "~/assets/js/plugins/bootstrap.min.js",
                    "~/assets/js/plugins/modernizr-2.8.3-respond-1.4.2.min.js",
                    "~/assets/js/plugins/revolution/js/jquery.themepunch.tools.min.js",
                    "~/assets/js/plugins/revolution/js/jquery.themepunch.revolution.min.js",
                    "~/assets/js/plugins/owl-carousel/owl.carousel.min.js",
                    "~/assets/js/plugins/parallax.min.js",
                    "~/assets/js/plugins/scrollReveal.min.js",
                    "~/assets/js/plugins/bootstrap-dropdownhover.min.js",
                    "~/assets/js/main.js",
                    "~/extensions/revolution.extension.parallax.min.js",
                    "~/extensions/revolution.extension.migration.min.js",
                    "~/extensions/revolution.extension.navigation.min.js",
                    "~/extensions/revolution.extension.kenburn.min.js",
                    "~/extensions/revolution.extension.layeranimation.min.js",
                    "~/extensions/revolution.extension.actions.min.js",
                    "~/extensions/revolution.extension.slideanims.min.js",
                    "~/extensions/revolution.extension.video.min.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                    "~/assets/js/plugins/owl-carousel/owl.carousel.min.js",
                    "~/assets/js/plugins/parallax.min.js",
                    "~/assets/js/plugins/scrollReveal.min.js",
                    "~/assets/js/plugins/bootstrap-dropdownhover.min.js",
                    "~/assets/js/main.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/extensions").Include(
                    "~/extensions/revolution.extension.parallax.min.js",
                    "~/extensions/revolution.extension.migration.min.js",
                    "~/extensions/revolution.extension.navigation.min.js",
                    "~/extensions/revolution.extension.kenburn.min.js",
                    "~/extensions/revolution.extension.layeranimation.min.js",
                    "~/extensions/revolution.extension.actions.min.js",
                    "~/extensions/revolution.extension.slideanims.min.js",
                    "~/extensions/revolution.extension.video.min.js"
                    ));

            BundleTable.EnableOptimizations = true;
        }
    }
}