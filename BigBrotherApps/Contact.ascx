﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Contact.ascx.cs" Inherits="BigBrother.Contact" %>
<div class="modal fade" id="ws-register-modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
            </div>

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">

                    <div class="modal-body">
                        <!-- Register Form -->
                        <div class="ws-register-form">

                            <h3>Free Quote in 24 hours!</h3>
                            <div class="ws-separator"></div>
                            <div class="form-group">
                                <asp:Label ID="spanMessage" Visible="false" CssClass="alert alert-success contactformmsg" runat="server"></asp:Label>
                            </div>
                            <div class="form-group">

                                <label class="control-label">Name <span>*</span></label>

                                <asp:TextBox ID="txtName" MaxLength="50" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqv" CssClass="red"
                                    ErrorMessage="This field is required" Display="Dynamic" runat="server" ControlToValidate="txtName"></asp:RequiredFieldValidator>
                            </div>

                            <!-- Email -->
                            <div class="form-group">
                                <label class="control-label">Email <span>*</span></label>

                                <asp:TextBox ID="txtEmail" MaxLength="50" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" CssClass="red" ID="RequiredFieldValidator3"
                                    runat="server" ControlToValidate="txtEmail" ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator Display="Dynamic" CssClass="red" ID="RegularExpressionValidator1" runat="server"
                                    ControlToValidate="txtEmail" ValidationExpression="^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"
                                    ErrorMessage="Email address invalid"></asp:RegularExpressionValidator>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Phone <span>*</span></label>

                                <asp:TextBox ID="txtContactNumber" MaxLength="20" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="red" ID="RequiredFieldValidator1" ControlToValidate="txtContactNumber"
                                    runat="server" Display="Dynamic" ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator Display="Dynamic" CssClass="red" ValidationExpression="\d+"
                                    ControlToValidate="txtContactNumber" ID="RegularExpressionValidator3" runat="server"
                                    ErrorMessage="Phone Number Invalid"></asp:RegularExpressionValidator>
                            </div>
                            <div class="form-group">
                                <label class="control-label">City / Area <span>*</span></label>
                                <asp:TextBox ID="txtAddress" MaxLength="500" class="form-control" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="red" ID="RequiredFieldValidator2" ControlToValidate="txtAddress"
                                    runat="server" Display="Dynamic" ErrorMessage="This field is required"></asp:RequiredFieldValidator>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Upload Docs</label><br />
                                <asp:FileUpload ID="fulAttachment" class="form-control" runat="server" />
                            </div>
                            <!-- Message -->
                            <div class="form-group">
                                <label class="control-label">Message <span>*</span></label>
                                <asp:TextBox TextMode="MultiLine" MaxLength="1000" class="form-control" ID="txtMessage"
                                    runat="server"></asp:TextBox>

                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator4"
                                    ControlToValidate="txtMessage" CssClass="red" runat="server" ErrorMessage="This field is required"></asp:RequiredFieldValidator>

                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>

                                        <asp:CheckBox ID="chkConsultancy" Text="I want free 30 minutes <a target='blank'  href='IT_Consultancy'>consultancy</a>" runat="server" />

                                    </label>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="form-group">
                            <asp:Button ID="btnSubmit" CssClass="btn ws-big-btn"
                                Text="Submit" runat="server" OnClick="btnSubmit_Click" />



                        </div>


                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
