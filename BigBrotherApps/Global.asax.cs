﻿using BigBrother.App_Start;
using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;

namespace BigBrother
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!HttpContext.Current.Request.Url.ToString().Contains("WebServiceSite.asmx") &&
                !HttpContext.Current.Request.Url.ToString().Contains("WebResource.axd") &&
                !HttpContext.Current.Request.Url.ToString().Contains("StripePaymentResponse") &&
                Regex.IsMatch(HttpContext.Current.Request.Url.ToString(), @"[A-Z]") == true)
            {
                string lower = HttpContext.Current.Request.Url.ToString().ToLower();

                HttpContext.Current.Response.Clear();

                HttpContext.Current.Response.Status = "301 Moved Permanently";

                HttpContext.Current.Response.AddHeader("Location", lower);

                HttpContext.Current.Response.End();
            }

            //if (!Request.IsSecureConnection && !Request.IsLocal)
            //{
            //    if (Request.Url.AbsoluteUri.ToUpper().StartsWith("HTTP://WWW"))
            //        Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
            //    else
            //        Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://www."));
            //}
            //else if (!Request.Url.AbsoluteUri.ToUpper().StartsWith("HTTPS://WWW") && !Request.IsLocal)
            //{
            //    Response.Redirect(Request.Url.AbsoluteUri.Replace("https://", "https://www."));
            //}
        }
    }
}