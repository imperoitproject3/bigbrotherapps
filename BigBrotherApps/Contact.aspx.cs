﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigBrother
{
    public partial class Contact1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ImperoIT.Inquirys ObjInquiry = new ImperoIT.Inquirys();
            string Name = txtName.Text.Trim();
            string Email = txtEmail.Text.Trim();
            string Address = txtAddress.Text.Trim();
            string Number = txtContactNumber.Text.Trim();
            string Message = txtMessage.Text.Trim();
            string FilePath = "";
            if (fulAttachment.HasFile)
            {
                string FileName = fulAttachment.FileName;
                string Path = Server.MapPath("Attachment");
                fulAttachment.SaveAs(Path + "\\" + FileName);
                ObjInquiry.Attachment = FileName;
                FilePath = Path + "\\" + FileName;
            }
            ObjInquiry.Name = Name;
            ObjInquiry.EmailAddress = Email;
            ObjInquiry.Address = Address;
            ObjInquiry.Message = Message;
            ObjInquiry.Number = Number;
            ImperoIT.Email ObjEMail = new ImperoIT.Email();

            StringBuilder mailContent = new StringBuilder();
            mailContent.Append("Person With Name: " + Name + " has contacted you on your website www.BigBrother.ie");
            mailContent.Append("<BR/>");
            mailContent.Append("<ul>");
            //mailContent.Append("<li>IT Consultancy Required: " + (chkConsultancy.Checked ? "Yes" : "No") + "</li>");
            mailContent.Append("<li>Phone number: " + Number + "</li>");
            mailContent.Append("<li>Email: " + Email + "</li>");
            mailContent.Append("<li>Location: " + Address + "</li>");
            mailContent.Append("<li>Message: " + Message + "</li>");
            mailContent.Append("</ul>");
            mailContent.Append("<BR/>");
            mailContent.Append("<BR/>");
            mailContent.Append("Thank You!");

            ObjEMail.SendEmail(mailContent, "notification@imperoit.com", "raza@imperoit.com", "mena.theodorou@gmail.com", "", "New Inquiry on www.bigbrotherapps.com.au", FilePath);            

            int success = ObjInquiry.AddInquiry();

            if (success == 1)
            {
                txtMessage.Text = txtAddress.Text = txtContactNumber.Text = txtName.Text = txtEmail.Text = "";
                //chkConsultancy.Checked = false;
                spanMessage.Visible = true;
                spanMessage.Text = "Thanks for your Inquiry. We shall be in touch with you within 24 working hours.";
            }
        }

        [WebMethod]
        public static bool IsReCaptchValid(string captchaResponse)
        {
            string secretKey = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SecretKey");
            var result = false;
            var apiUrl = "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}";
            var requestUri = string.Format(apiUrl, secretKey, captchaResponse);
            var request = (HttpWebRequest)WebRequest.Create(requestUri);

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    JObject jResponse = JObject.Parse(stream.ReadToEnd());
                    var isSuccess = jResponse.Value<bool>("success");
                    result = (isSuccess) ? true : false;
                }
            }
            return result;
        }

    }
}