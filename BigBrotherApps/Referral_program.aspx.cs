﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BigBrother
{
    public partial class Referral_Program : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ImperoIT.Inquirys ObjInquiry = new ImperoIT.Inquirys();
            string Name = txtName.Text.Trim();
            string Email = txtEmail.Text.Trim();
            string Address = txtlocation.Text.Trim();
            string Number = txtContactNumber.Text.Trim();
            string Remarks = txtRemark.Text.Trim();

            string ReferralName = txtReferralName.Text.Trim();
            string ReferralEmail = txtReferralEmail.Text.Trim();
            string ReferralPhone = txtReferralPhone.Text.Trim();

            string Message = string.Empty;
            if (drdRefer.SelectedValue == "1")
            {
                Message = string.Format("We're an IT Company and referring the following person: \n\n Name: {0} \n\n Email: {1} \n\n Phone Number: {2}", ReferralName, ReferralEmail, ReferralPhone);
            }
            else if (drdRefer.SelectedValue == "2")
            {
                Message = string.Format("I'm an Individual and referring the following person: \n\n Name: {0} \n\n Email: {1} \n\n Phone Number: {2}", ReferralName, ReferralEmail, ReferralPhone);
            }

            ObjInquiry.Name = Name;
            ObjInquiry.EmailAddress = Email;
            ObjInquiry.Address = Address;
            ObjInquiry.Message = Message;
            ObjInquiry.Number = Number;
            ObjInquiry.Remark = Remarks;

            ImperoIT.Email ObjEMail = new ImperoIT.Email();

            StringBuilder mailContent = new StringBuilder();
            mailContent.Append("<B>" + Name + "</B> has sent a new reference through BigBrother website.");
            mailContent.Append("<BR/>");
            mailContent.Append("<BR/>");
            mailContent.Append(Name + " is an " + drdRefer.SelectedItem.Text + " and the contact details are:");
            mailContent.Append("<BR/>");
            mailContent.Append("<ul>");
            mailContent.Append("<li>Phone number: " + Number + "</li>");
            mailContent.Append("<li>Email: " + Email + "</li>");
            mailContent.Append("<li>Location: " + Address + "</li>");
            mailContent.Append("<li>Remarks: " + Remarks + "</li>");
            mailContent.Append("</ul>");
            mailContent.Append("Referral's contact details are:");
            mailContent.Append("<ul>");
            mailContent.Append("<li>Name: " + ReferralName + "</li>");
            mailContent.Append("<li>Email: " + ReferralEmail + "</li>");
            mailContent.Append("<li>Phone number: " + ReferralPhone + "</li>");
            mailContent.Append("</ul>");
            mailContent.Append("<BR/>");
            mailContent.Append("Thank You!");

            ObjEMail.SendEmail(mailContent, "notification@imperoit.com", "info@BigBrother.ie", "razanoorani.it@gmail.com, raza@imperoit.com, stoneciaran@gmail.com,juned.ghanchi@gmail.com", "", "New referral on www.BigBrother.ie", string.Empty);

            int success = ObjInquiry.AddInquiry();

            if (success == 1)
            {
                txtRemark.Text = txtlocation.Text = txtContactNumber.Text = txtName.Text = txtEmail.Text = txtReferralName.Text = txtReferralEmail.Text = txtReferralPhone.Text = string.Empty;
                spanMessage.Visible = true;
                spanMessage.Text = "Thanks for the reference. We shall be in touch with " + ReferralName + " within 24 working hours.";
            }
        }
    }
}